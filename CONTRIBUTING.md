# Contributing to Bow Diary

First off, thank you for considering contributing to Bow Diary. It's people like you that make Bow Diary such a great tool.

## Where do I go from here?

If you've noticed a bug or have a feature request, make sure to check our [Issues](https://github.com/[username]/[repository]/issues) to see if someone else in the community has already created a ticket. If not, go ahead and [make one](https://github.com/[username]/[repository]/issues/new)!

## Fork & create a branch

If this is something you think you can fix, then fork the repository and create a branch with a descriptive name.

A good branch name would be (where issue #325 is the ticket you're working on):

```bash
git checkout -b 325-add-japanese-localisation
