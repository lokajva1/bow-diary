# API

## Description

This is a Bachelor's thesis project developed with Spring Boot and React. This project provides basic functionality for
archery bow diary. The application allows users to log their archery training sessions, track their progress, and
analyze their performance.
The API is a RESTful web service that provides endpoints for managing user accounts, user data, and user preferences.
The API is developed with Spring Boot and React, and uses a PostgreSQL database.

## Getting Started

### Prerequisites

- Java 18
- Gradle
- Docker Desktop 4.28.0
- Docker Compose v2.24.6-desktop.1

### Local Development

For local development use IntelliJ IDEA or similar IDE. For running the application locally, use the preconfigured
Docker Compose setup.

### Installation

1. Navigate to the project directory

```bash
cd api
```

2. Build the project using IntelliJ IDEA Gradle plugin, or run the following command

```bash
gradle build
```

3. Run the preconfigured Docker Compose setup using IntelliJ IDEA Docker plugin or the following command

```bash
docker-compose up
```

4. Run the application using IntelliJ IDEA or the following command

```bash
gradle bootRun
```

## Usage

The API will be available at `http://localhost:8080`. Postman collection with example requests is available in
the `postman` directory.

## Support

For any queries, please open an issue in the GitHub repository.

## Container Availability Notice
As of June 30, 2024, the container deployed in DigitalOcean has been stopped and will no longer be available on OPE.

# App

## Description

This is a React Native application developed with TypeScript and managed with npm.

## Getting Started

### Prerequisites

- Java 18
- Node.js v20.12.2
- npm 10.5.0

### Local Development

For local development use Android Studio Iguana or similar IDE. For running the application on an emulator, use Android
Studio or the Android Emulator.
Developed for Android version 5.0 and newer. But during development were used Android Emulator with Android Version 13 (
Virtual device Pixel 3a API TiramisuPrivacySandbox).

### Installation

1. Navigate to the project directory

```bash
cd app
```

3. Install the dependencies

```bash
npm install
```

4. Run Android emulator

5. Start the application for an Android emulator

```bash
npm run android
```

## Usage

The application will be available on your local device or emulator. For default user username is `username` and password
is `password`.

## Support

For any queries, please open an issue in the GitHub repository.

## Contributing

Contributions are welcome. Please open a pull request with your changes.

## License

This project is licensed under the MIT License.

## Project status

This project is currently in development.
