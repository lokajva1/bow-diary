import React from 'react';
import {StyleProp, StyleSheet, Text, View, ViewStyle} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import sharedStyles from '../../assets/style/SharedStyles.tsx';

/**
 * CheckBoxComponentProps is an interface that defines the props for the CheckBoxInputComponent.
 *
 * @interface
 * @property {string} label - The label text displayed next to the checkbox.
 * @property {StyleProp<ViewStyle>} viewStyle - The style applied to the View component that wraps the checkbox and label.
 * @property {boolean} isSelected - The current state of the checkbox, whether it is selected or not.
 * @property {(value: boolean) => void} setSelection - The function that updates the state of the checkbox when it is clicked.
 */
interface CheckBoxComponentProps {
  label: string;
  viewStyle: StyleProp<ViewStyle>;
  isSelected: boolean;
  setSelection: (value: boolean) => void;
}
/**
 * CheckBoxInputComponent is a functional component that renders a checkbox input along with a label.
 * It uses the CheckBox component from the @react-native-community/checkbox package.
 * The component receives its props in the form of the CheckBoxComponentProps interface.
 *
 * @component
 * @param {string} label - The label text displayed next to the checkbox.
 * @param {StyleProp<ViewStyle>} viewStyle - The style applied to the View component that wraps the checkbox and label.
 * @param {boolean} isSelected - The current state of the checkbox, whether it is selected or not.
 * @param {(value: boolean) => void} setSelection - The function that updates the state of the checkbox when it is clicked.
 *
 * @example
 * <CheckBoxInputComponent label="Example" viewStyle={styles.exampleStyle} isSelected={this.state.isSelected} setSelection={this.setSelection} />
 */
const CheckBoxInputComponent= ({ label, viewStyle, isSelected, setSelection }: CheckBoxComponentProps) => {
  return (
    <View style={viewStyle}>
      <View style={styles.checkboxContainer}>
        <CheckBox
          value={isSelected}
          onValueChange={setSelection}
          style={styles.checkbox}
        />
      </View>
      <Text style={styles.label} numberOfLines={1}>{label}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  checkboxContainer: {
    borderColor: SharedStyles.darkGrey,
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    marginTop: 6,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 8,
    paddingBottom: 8
  },
  checkbox: {
    alignSelf: 'center'
  },
  label: {
    fontSize: 11,
    position: 'absolute',
    backgroundColor: sharedStyles.white,
    marginLeft: 15
  }
});

export default CheckBoxInputComponent;
