import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import sharedStyles from '../../assets/style/SharedStyles.tsx';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {TargetFaceModel} from '../../models/TargetFaceModel.tsx';

/**
 * TargetFaceComponentProps is an interface that defines the props for the TargetFaceComponent.
 *
 * @interface
 * @property {TargetFaceModel} targetFace - The TargetFaceModel object that contains the data for the target face.
 */
interface TargetFaceComponentProps {
  targetFace: TargetFaceModel;
}

/**
 * TargetFaceComponent is a functional component that renders a target face.
 *
 * @component
 * @param {TargetFaceModel} targetFace - The TargetFaceModel object that contains the data for the target face.
 *
 * @example
 * <TargetFaceComponent targetFace={exampleTargetFace} />
 */
const TargetFaceComponent = ({ targetFace }: TargetFaceComponentProps) => {
  return (
    <View>
      <View style={styles.rowContainer}>
        <Image source={require('../../assets/images/wa_80_target_face.png')}
               style={styles.targetFace} />
        <View style={styles.infoView}>
          <Text>{targetFace.name} {targetFace.diameter} cm</Text>
          <Text>{targetFace.type}</Text>
        </View>
        <Icon name="edit" size={40} color={SharedStyles.black}></Icon>
      </View>
      <Text style={styles.labelHeader} numberOfLines={1}>Target face</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  rowContainer: {
    borderColor: SharedStyles.darkGrey,
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    marginTop: 6,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 8,
    paddingBottom: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
    gap: 10
  },
  infoView: {
    flexGrow: 1
  },
  labelHeader: {
    fontSize: 11,
    position: 'absolute',
    backgroundColor: sharedStyles.white,
    marginLeft: 15
  },
  touchToSelect: {
    fontSize: 50,
    color: sharedStyles.grey,
    position: 'absolute',
    width: '100%',
    marginTop: 10,
    textAlign: 'center'
  },
  notSelectedBackground: {
    backgroundColor: sharedStyles.darkGrey
  },
  targetFace: {
    width: 50,
    height: 50,
    marginRight: 10
  }
});

export default TargetFaceComponent;
