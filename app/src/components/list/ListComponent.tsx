import React from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
/**
 * ListComponent is a functional component that renders a list of views.
 * It uses the FlatList component from the react-native library.
 *
 * @component
 * @param {JSX.Element[]} views - An array of JSX elements to be rendered in the list.
 *
 * @example
 * <ListComponent views={[<View1 />, <View2 />, <View3 />]} />
 */
const ListComponent = ({ views }: { views: JSX.Element[] }) => {
  /**
   * renderItem is a helper function that wraps each view in a styled View component.
   *
   * @function
   * @param {JSX.Element} item - A JSX element to be rendered in the list.
   * @returns {JSX.Element} A styled View component containing the input JSX element.
   */
  const renderItem = ({ item }: { item: JSX.Element }): JSX.Element => (
    <View style={styles.item}>
      {item}
    </View>
  );

  return (
    <FlatList
      data={views}
      renderItem={renderItem}
      keyExtractor={(_, index) => index.toString()}
      contentContainerStyle={styles.list}
    />
  );
};

const styles = StyleSheet.create({
  list: {
    marginTop: 10,
  },
  item: {
    padding: 10,
    marginTop: 5,
    marginRight: 5,
    marginBottom: 5,
    marginLeft: 5,
    backgroundColor: SharedStyles.white,
    borderRadius: 5,
  },
});

export default ListComponent;
