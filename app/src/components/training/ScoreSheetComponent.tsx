import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import {ScoreSheetModel} from '../../models/ScoreSheetModel.tsx';
import {ArrowPosition, ArrowPositionImpl} from '../../models/ArrowPosition.tsx';

/**
 * ScoreSheetComponentProps is an interface that defines the props for the ScoreSheetComponent.
 *
 * @interface
 * @property {ScoreSheetModel} scoreSheet - The ScoreSheetModel object that contains the data for the score sheet.
 */
interface ScoreSheetComponentProps {
  scoreSheet: ScoreSheetModel;
}

/**
 * ScoreSheetComponent is a functional component that renders a score sheet.
 *
 * @component
 * @param {ScoreSheetModel} scoreSheet - The ScoreSheetModel object that contains the data for the score sheet.
 *
 * @example
 * <ScoreSheetComponent scoreSheet={exampleScoreSheet} />
 */
const ScoreSheetComponent = ({ scoreSheet }: ScoreSheetComponentProps) => {

  /**
   * renderScoreBox is a helper function that renders the score boxes for the score sheet.
   *
   * @function
   * @param {ArrowPosition[]} arrowPositions - An array of ArrowPosition objects that represent the positions of the arrows on the score sheet.
   * @returns {JSX.Element} A JSX element that represents the score boxes for the score sheet.
   */
  const renderScoreBox = (arrowPositions: ArrowPosition []): JSX.Element => {
    const extendedArrowPositions = [...arrowPositions];
    while (extendedArrowPositions.length < scoreSheet.maxArrows) {
      extendedArrowPositions.push(new ArrowPositionImpl('', 0, 0, ' '));
    }
    if (scoreSheet.maxArrows < 4) {
      return (
        <View style={styles.scoreContainer}>
          <View style={styles.scoreBoxRow}>
            {extendedArrowPositions.map((arrowPosition, idx) => (
              <Text key={idx} style={styles.scoreBoxText}>{arrowPosition.scoreLabel}</Text>
            ))}
          </View>
        </View>
      );
    }
    const firstHalfArrowPositions = extendedArrowPositions.slice(0, Math.ceil(extendedArrowPositions.length / 2));
    const secondHalfArrowPositions = extendedArrowPositions.slice(Math.ceil(extendedArrowPositions.length / 2));
    return (
      <View style={styles.scoreContainer}>
        <View style={styles.scoreBoxRow}>
          {firstHalfArrowPositions.map((arrowPosition, idx) => (
            <Text key={idx} style={styles.scoreBoxText}>{arrowPosition.scoreLabel}</Text>
          ))}
        </View>
        <View style={styles.scoreBoxRow}>
          {secondHalfArrowPositions.map((arrowPosition, idx) => (
            <Text key={idx} style={styles.scoreBoxText}>{arrowPosition.scoreLabel}</Text>
          ))}
          {extendedArrowPositions.length % 2 === 1 &&
            <Text key={"hidden_text"} style={[styles.scoreBoxText, styles.hidden]}>0</Text>
          }
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.roundContainer}>
        <Text style={styles.roundBoxText}>{scoreSheet.roundEndPosition}</Text>
      </View>
      {renderScoreBox(scoreSheet.arrowPositions)}
      <View style={styles.totalScoreContainer}>
        <View style={styles.totalScoreRow}>
          <Text style={styles.totalScoreText}>{scoreSheet.getScore()}</Text>
          <Text style={styles.totalScoreText}>{scoreSheet.getTotalScore()}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row'
  },
  roundContainer: {
    flexGrow: 1
  },
  roundBoxText: {
    fontSize: 20,
    borderColor: SharedStyles.black,
    borderWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    textAlign: 'center',
    backgroundColor: SharedStyles.white
  },
  scoreBoxText: {
    fontSize: 30,
    borderColor: SharedStyles.black,
    borderWidth: 1,
    textAlign: 'center',
    flexGrow: 1,
    backgroundColor: SharedStyles.white
  },
  scoreBoxRow: {
    flexDirection: 'row',
    flexGrow: 1
  },
  scoreContainer: {
    flexGrow: 10
  },
  totalScoreContainer: {
    flexGrow: 5
  },
  totalScoreRow: {
    flexDirection: 'row'
  },
  totalScoreText: {
    fontSize: 25,
    borderColor: SharedStyles.black,
    borderWidth: 1,
    paddingLeft: 10,
    paddingRight: 10,
    textAlign: 'center',
    flexGrow: 1,
    backgroundColor: SharedStyles.white
  },
  hidden: {
    opacity: 0,
  }
});

export default ScoreSheetComponent;
