import React from 'react';
// @ts-ignore
import { Grid, LineChart, XAxis, YAxis } from 'react-native-svg-charts'
import { View } from 'react-native'

/**
 * BarChartComponentProps is an interface that defines the props for the LineChartComponent.
 *
 * @interface
 * @property {number[]} data - An array of numbers that represents the data for the line chart.
 * @property {string[]} headers - An array of strings that represents the headers for the line chart.
 */
interface BarChartComponentProps {
  data: number[];
  headers: string[];
}

/**
 * LineChartComponent is a functional component that renders a line chart.
 *
 * @component
 * @param {number[]} data - An array of numbers that represents the data for the line chart.
 * @param {string[]} headers - An array of strings that represents the headers for the line chart.
 *
 * @example
 * <LineChartComponent data={[10, 20, 30]} headers={['Jan', 'Feb', 'Mar']} />
 */
const LineChartComponent = ({ data, headers }: BarChartComponentProps) => {
  return (
    <View style={{ flexGrow: 1, padding: 10, flexDirection: 'row' }}>
      <YAxis
        data={data}
        style={{ marginBottom: 10 }}
        contentInset={{ top: 10, bottom: 10 }}
        svg={{ fontSize: 10, fill: 'grey' }}
      />
      <View style={{ flex: 1, marginLeft: 10 }}>
        <LineChart
          style={{ flex: 1 }}
          data={data}
          contentInset={{ top: 10, bottom: 10 }}
          svg={{ stroke: 'rgb(134, 65, 244)' }}
        >
          <Grid/>
        </LineChart>
        <XAxis
          style={{ marginHorizontal: -10, height: 10 }}
          data={data}
          formatLabel={(value: string, index: number) => headers[index]}
          contentInset={{ left: 10, right: 10 }}
          svg={{ fontSize: 10, fill: 'grey' }}
        />
      </View>
    </View>
  )
};

export default LineChartComponent;
