import React from 'react';
// @ts-ignore
import {BarChart, XAxis} from 'react-native-svg-charts';
import {Text as SVGText} from 'react-native-svg';
import {View} from 'react-native';
import SharedStyles from '../../assets/style/SharedStyles.tsx';

/**
 * LabelsProps is an interface that defines the props for the Labels component.
 *
 * @interface
 * @property {(index: number) => number} x - A function that takes an index and returns a number.
 * @property {(value: number) => number} y - A function that takes a value and returns a number.
 * @property {number} bandwidth - The bandwidth for the bar chart.
 * @property {number[]} data - An array of numbers that represents the data for the bar chart.
 */
interface LabelsProps {
  x: (index: number) => number;
  y: (value: number) => number;
  bandwidth: number;
  data: number[];
}

/**
 * BarChartComponentProps is an interface that defines the props for the BarChartComponent.
 *
 * @interface
 * @property {number[]} data - An array of numbers that represents the data for the bar chart.
 * @property {string[]} headers - An array of strings that represents the headers for the bar chart.
 */
interface BarChartComponentProps {
  data: number[];
  headers: string[];
}

/**
 * BarChartComponent is a functional component that renders a bar chart.
 *
 * @component
 * @param {number[]} data - An array of numbers that represents the data for the bar chart.
 * @param {string[]} headers - An array of strings that represents the headers for the bar chart.
 *
 * @example
 * <BarChartComponent data={[10, 20, 30]} headers={['Jan', 'Feb', 'Mar']} />
 */
const BarChartComponent = ({ data, headers }: BarChartComponentProps) => {
  /**
   * Labels is a functional component that renders the labels for the bar chart.
   *
   * @component
   * @param {(index: number) => number} x - A function that takes an index and returns a number.
   * @param {(value: number) => number} y - A function that takes a value and returns a number.
   * @param {number} bandwidth - The bandwidth for the bar chart.
   * @param {number[]} data - An array of numbers that represents the data for the bar chart.
   */
  const Labels = ({ x, y, bandwidth }: LabelsProps) => (
    data.map((value: number, index: number) => (
      <SVGText
        key={index}
        x={x(index) + (bandwidth / 2)}
        y={y(value) + 15}
        fill={SharedStyles.white}
        alignmentBaseline={'middle'}
        textAnchor={'middle'}
      >
        {value}
      </SVGText>
    ))
  );

  return (
    <View style={{ flexDirection: 'column', flexGrow: 1 }}>
      <BarChart
        style={{ flex: 1 }}
        data={data}
        svg={{ fill: 'rgba(134, 65, 244, 0.8)' }}
        contentInset={{ top: 10, bottom: 10 }}
        gridMin={0}
      >
        {/*error is deprecated because of logic is handled in BarChart itself*/}
        <Labels />
      </BarChart>
      <XAxis
        contentInset={{ left: 18, right: 18 }}
        data={data}
        formatLabel={(value: string, index: number) => headers[index]}
      />
    </View>
  );
};

export default BarChartComponent;
