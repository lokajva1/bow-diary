import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import sharedStyles from '../../assets/style/SharedStyles.tsx';

/**
 * WeatherComponent is a functional component that renders a weather information box.
 *
 * @component
 *
 * @example
 * <WeatherComponent />
 */
const WeatherComponent = () => {
  return (
    <View >
      <View style={styles.rowContainer}>
        <Icon name="sunny" size={40} color={SharedStyles.black}></Icon>
        <View style={styles.infoView}>
          <Text>Wind</Text>
          <Text>0 Bf</Text>
          <Text>from front</Text>
        </View>
        <Icon name="edit" size={40} color={SharedStyles.black}></Icon>
      </View>
      <Text style={styles.labelHeader} numberOfLines={1}>Weather</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  rowContainer: {
    borderColor: SharedStyles.darkGrey,
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    marginTop: 6,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 8,
    paddingBottom: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
    gap: 10
  },
  infoView: {
    flexGrow: 1
  },
  labelHeader: {
    fontSize: 11,
    position: 'absolute',
    backgroundColor: sharedStyles.white,
    marginLeft: 15
  },
});
export default WeatherComponent;
