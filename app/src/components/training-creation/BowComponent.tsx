import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import sharedStyles from '../../assets/style/SharedStyles.tsx';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {BowModel} from '../../models/BowModel.tsx';

/**
 * TrainingComponentProps is an interface that defines the props for the BowComponent.
 *
 * @interface
 * @property {BowModel} bow - The BowModel object that contains the data for the bow.
 * @property {boolean} isSelected - A boolean that indicates whether the bow is selected.
 */
interface TrainingComponentProps {
  bow: BowModel;
  isSelected: boolean;
}

/**
 * BowComponent is a functional component that renders a bow.
 *
 * @component
 * @param {BowModel} bow - The BowModel object that contains the data for the bow.
 * @param {boolean} isSelected - A boolean that indicates whether the bow is selected.
 *
 * @example
 * <BowComponent bow={exampleBow} isSelected={true} />
 */
const BowComponent = ({ bow,  isSelected }: TrainingComponentProps) => {
  return (
    <View >
      <View style={isSelected ? styles.rowContainer : [styles.rowContainer, styles.notSelectedBackground]}>
        <Image source={require('../../assets/images/regualar_bow.png')}
               style={styles.bowImage} />
        <View style={styles.infoView}>
          <Text>{bow.name}</Text>
          <Text>{bow.bowType.name}</Text>
          <Text>{bow.strength} lbs</Text>
        </View>
        <Icon name="edit" size={40} color={SharedStyles.black}></Icon>
      </View>
      {isSelected || <Text style={styles.touchToSelect} numberOfLines={1}>Touch to select</Text>}
      <Text style={styles.labelHeader} numberOfLines={1}>Bow</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  rowContainer: {
    borderColor: SharedStyles.darkGrey,
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    marginTop: 6,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 8,
    paddingBottom: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
    gap: 10
  },
  infoView: {
    flexGrow: 1
  },
  labelHeader: {
    fontSize: 11,
    position: 'absolute',
    backgroundColor: sharedStyles.white,
    marginLeft: 15
  },
  touchToSelect: {
    fontSize: 50,
    color: sharedStyles.grey,
    position: 'absolute',
    width: '100%',
    marginTop: 10,
    textAlign: 'center',
  },
  notSelectedBackground: {
    backgroundColor: sharedStyles.darkGrey
  },
  bowImage: {
    width: 50,
    height: 50,
    marginRight: 10
  }
});

export default BowComponent;
