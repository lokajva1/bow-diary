import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import sharedStyles from '../../assets/style/SharedStyles.tsx';
import {TrainingRoundModel} from '../../models/TrainingRoundModel.tsx';
import Icon from 'react-native-vector-icons/MaterialIcons';

/**
 * TrainingComponentProps is an interface that defines the props for the RoundComponent.
 *
 * @interface
 * @property {TrainingRoundModel} trainingRound - The TrainingRoundModel object that contains the data for the training round.
 */
interface TrainingComponentProps {
  trainingRound: TrainingRoundModel;
}

/**
 * RoundComponent is a functional component that renders a round of training.
 *
 * @component
 * @param {TrainingRoundModel} trainingRound - The TrainingRoundModel object that contains the data for the training round.
 *
 * @example
 * <RoundComponent trainingRound={exampleTrainingRound} />
 */
const RoundComponent = ({ trainingRound }: TrainingComponentProps) => {
  return (
    <View>
      <View style={styles.rowContainer}>
        <Image source={require('../../assets/images/wa_80_target_face.png')}
               style={{ width: 50, height: 50, marginRight: 10 }} />
        <View style={styles.infoView}>
          <Text>{trainingRound.targetFace.name} {trainingRound.targetFace.diameter} cm</Text>
          <Text>{trainingRound.targetFace.type}</Text>
        </View>
        <View>
          <Text>{trainingRound.numberOfEnds}x{trainingRound.arrowInEachEnd}</Text>
          <Text>{trainingRound.distance} m</Text>
        </View>
        <Icon name="edit" size={40} color={SharedStyles.black}></Icon>
      </View>
      <Text style={styles.label} numberOfLines={1}>Round {trainingRound.roundNumber}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  rowContainer: {
    borderColor: SharedStyles.darkGrey,
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    marginTop: 6,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 8,
    paddingBottom: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
    gap: 10
  },
  infoView: {
    flexGrow: 1
  },
  label: {
    fontSize: 11,
    position: 'absolute',
    backgroundColor: sharedStyles.white,
    marginLeft: 15
  }
});

export default RoundComponent;
