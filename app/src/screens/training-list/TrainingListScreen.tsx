import {useEffect, useLayoutEffect, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {TrainingModel} from '../../models/TrainingModel.tsx';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import ListComponent from '../../components/list/ListComponent.tsx';
import DateService from '../../services/common/DateService.tsx';
import {useIsFocused} from '@react-navigation/native';
import UserAccountService from '../../services/api/UserAccountService.tsx';
import NavigationService from '../../services/common/NavigationReferenceService.tsx';

/**
 * TrainingListScreen is a React component that displays a list of trainings.
 * The user can select a training from the list, which will then navigate to the 'Training Rounds' screen.
 *
 * @param {object} navigation - The navigation object provided by React Navigation.
 * @returns {JSX.Element} A React component.
 */
const TrainingListScreen = ({ navigation }: any) => {
  /**
   * isFocused is a boolean that indicates whether the screen is currently focused.
   */
  const isFocused = useIsFocused();

  /**
   * trainings is the current list of TrainingModel objects.
   * setTrainings is a function to update the list of TrainingModel objects.
   */
  const [trainings, setTrainings] = useState<TrainingModel[]>([]);

  /**
   * This effect fetches the active user id and the corresponding trainings when the component is focused.
   * If there is no active user id, it navigates to the 'Login' screen.
   */
  useEffect(() => {
    UserAccountService.getActiveUserId()
      .then((userId) => {
        console.log('using userId: ' + userId);
          if (!userId) {
            NavigationService.navigate('Login');
          } else {
            UserAccountService.getUserTrainings(userId)
              .then((fetchedTrainings) => {
                setTrainings(fetchedTrainings);
              })
              .catch((error) => console.error(error));
          }
        }
      ).catch((error) => console.error(error));
  }, [isFocused]);

  /**
   * This effect sets the header left button to be the 'Menu' icon and the header right button to be the 'Settings' icon.
   * When the 'Menu' icon is clicked, it warns that the 'Menu' is not implemented.
   * When the 'Settings' icon is clicked, it warns that the 'Settings' is not implemented.
   */
  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => {
        return <Icon name="menu" size={30} color={SharedStyles.white} onPress={() => console.warn('Menu is not implemented!')}/>;
      },
      headerRight: () => {
        return <Icon name="settings" size={30} color={SharedStyles.white} onPress={() => console.warn('Settings is not implemented!')} />;
      }
    });
  }, [navigation]);

  return (
    <View style={styles.view}>
      <ListComponent views={trainings.map((training) => (
        <TouchableOpacity key={training.id} style={styles.itemRow}
                          onPress={() => NavigationService.navigate('Training Rounds', { trainingId: training.id })}>
          <Icon name="adjust" size={40} color={SharedStyles.black}></Icon>
          <View style={styles.headerView}>
            <Text style={styles.header}>{training.name}</Text>
            <Text>{DateService.toLocaleDateString(training.date)}</Text>
          </View>
          <View style={styles.scoreView}>
            <Text>{training.getActiveRound()}/{training.rounds.length}</Text>
            <Text>{training.getScore()}/{training.getMaxScore()}</Text>
          </View>
          <Icon name="edit" size={40} color={SharedStyles.black}></Icon>
        </TouchableOpacity>
      ))} />
      <TouchableOpacity onPress={() => NavigationService.navigate('Training Creation')} style={styles.addButton}>
        <Icon name="add" size={80} color={SharedStyles.black}></Icon>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flexGrow: 1
  },
  itemRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    gap: 10,
    alignItems: 'center'
  },
  headerView: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1
  },
  scoreView: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  header: {
    fontWeight: 'bold'
  },
  addButton: {
    backgroundColor: SharedStyles.yellow,
    borderRadius: 50,
    width: 80,
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 20,
    right: 20
  }
});


export default TrainingListScreen;
