import React, {useState} from 'react';
import {Button, StyleSheet, View} from 'react-native';
import UserAccountService from '../../services/api/UserAccountService.tsx';
import TokenService from '../../services/common/TokenService.tsx';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import {TextInput} from 'react-native-paper';
import {UserAccountModel, UserAccountModelImpl} from '../../models/UserAccountModel.tsx';
import NavigationService from '../../services/common/NavigationReferenceService.tsx';

/**
 * LoginScreen is a React component that provides a login interface for users.
 *
 * @returns {JSX.Element} A React component.
 */
function LoginScreen(): JSX.Element {
  /**
   * userAccount is the current UserAccountModel object that represents the user's account.
   * setUserAccount is a function to update the UserAccountModel object.
   */
  const [userAccount, setUserAccount] = useState<UserAccountModel>(new UserAccountModelImpl());

  /**
   * handleLogin is an asynchronous function that handles the login process.
   * It first checks if the username and password are between 3 and 20 characters long.
   * If they are, it calls the UserAccountService's generateToken method with the userAccount object.
   * If a token is successfully generated, it sets the token and the active user id, and navigates to the 'My Trainings' screen.
   * If an error occurs, it logs the error message.
   */
  const handleLogin = async () => {
    if (userAccount.username.length < 3 || userAccount.username.length > 20 ||
      userAccount.password.length < 3 || userAccount.password.length > 20) {
      console.warn('Username and password must be between 3 and 20 characters long');
      return;
    }
    console.log("Logging in");
    UserAccountService.generateToken(userAccount)
      .then(tokenModel => {
        console.log(tokenModel);
        TokenService.setToken(tokenModel.token)
        UserAccountService.setActiveUserId(tokenModel.userId)
        NavigationService.navigate('My Trainings');
      })
      .catch(error => {
        console.error(error.response.data);
      });
  };

  return (
    <View style={styles.container}>
      <View style={styles.loginBox}>
        <View style={styles.textInputContainer}>
          <TextInput
            label="Username"
            mode="outlined"
            activeOutlineColor={SharedStyles.black}
            value={userAccount.username}
            onChangeText={username => setUserAccount({...userAccount, username})}
          />
          <TextInput
            label="Password"
            mode="outlined"
            activeOutlineColor={SharedStyles.black}
            value={userAccount.password}
            onChangeText={password => setUserAccount({...userAccount, password})}
            secureTextEntry
          />
        </View>
        <Button
          title="Login"
          onPress={handleLogin}
          color={SharedStyles.green}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInputContainer: {
    gap: 5
  },
  loginBox: {
    width: '80%',
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    display: 'flex',
    gap: 10
  }
});

export default LoginScreen;
