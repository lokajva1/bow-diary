import React, {useEffect, useLayoutEffect, useState} from 'react';
import {TextInput} from 'react-native-paper';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import CheckBoxInputComponent from '../../components/inputs/CheckBoxInputComponent.tsx';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {TrainingModel, TrainingModelImpl, TrainingRequestModelImpl} from '../../models/TrainingModel.tsx';
import RoundComponent from '../../components/training-creation/RoundComponent.tsx';
import {
  TrainingRoundModel,
  TrainingRoundModelImpl,
  TrainingRoundRequestModelImpl
} from '../../models/TrainingRoundModel.tsx';
import BowComponent from '../../components/training-creation/BowComponent.tsx';
import {BowModel} from '../../models/BowModel.tsx';
import WeatherComponent from '../../components/training-creation/WeatherComponent.tsx';
import TrainingService from '../../services/api/TrainingService.tsx';
import UserAccountService from '../../services/api/UserAccountService.tsx';
import NavigationService from '../../services/common/NavigationReferenceService.tsx';

/**
 * TrainingCreationScreen is a React component that provides an interface for creating a new training.
 *
 * @param {object} navigation - The navigation object provided by React Navigation.
 * @returns {JSX.Element} A React component.
 */
const TrainingCreationScreen = ({ navigation }: any): JSX.Element => {
  /**
   * userId is the id of the current user.
   * setUserId is a function to update the id of the current user.
   */
  const [userId, setUserId] = useState<string>('');

  /**
   * training is the current TrainingModel object that represents the training being created.
   * setTraining is a function to update the TrainingModel object.
   */
  const [training, setTraining] = useState<TrainingModel>(new TrainingModelImpl());

  /**
   * isCreateTrainingClicked is a boolean that indicates whether the 'Create Training' button has been clicked.
   * setIsCreateTrainingClicked is a function to update the isCreateTrainingClicked state.
   */
  const [isCreateTrainingClicked, setIsCreateTrainingClicked] = useState(false);

  /**
   * isSelectedUseArrowNumbers is a boolean that indicates whether the 'Use Arrow Numbers' checkbox is selected.
   * setIsSelectedUseArrowNumbers is a function to update the isSelectedUseArrowNumbers state.
   */
  const [isSelectedUseArrowNumbers, setIsSelectedUseArrowNumbers] = useState(false);

  /**
   * isSelectedFreeTraining is a boolean that indicates whether the 'Free Training' checkbox is selected.
   * setIsSelectedFreeTraining is a function to update the isSelectedFreeTraining state.
   */
  const [isSelectedFreeTraining, setIsSelectedFreeTraining] = useState(false);

  /**
   * isSelectedUseClocks is a boolean that indicates whether the 'Use Clocks' checkbox is selected.
   * setIsSelectedUseClocks is a function to update the isSelectedUseClocks state.
   */
  const [isSelectedUseClocks, setIsSelectedUseClocks] = useState(false);

  /**
   * This effect fetches the active user id when the component is mounted.
   * If there is no active user id, it navigates to the 'Login' screen.
   * If the 'Create Training' button has been clicked, it calls the createTraining function.
   */
  useEffect(() => {
    UserAccountService.getActiveUserId()
      .then(userId => {
        if (!userId) {
          NavigationService.navigate('Login');
        } else {
          setUserId(userId);
        }
      })
      .catch(error => console.error(error));
    if (isCreateTrainingClicked) {
      setIsCreateTrainingClicked(false);
      createTraining();
    }
  }, [isCreateTrainingClicked]);

  /**
   * This effect sets the header right button to be the 'Done' icon.
   * When the 'Done' icon is clicked, it sets the isCreateTrainingClicked state to true.
   */
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return <Icon name="done" size={30} color={SharedStyles.white}
                     onPress={() => setIsCreateTrainingClicked(true)} />;
      }
    });
  }, [navigation]);

  /**
   * createTraining is a function that creates a new training.
   * It first checks if the training name, bow id, and rounds are not empty.
   * If they are not, it creates a new TrainingRequestModel object and calls the TrainingService's createUserTrainings method with the request payload.
   * If a training is successfully created, it navigates to the 'Training Rounds' screen.
   * If an error occurs, it logs the error message.
   */
  const createTraining = () => {
    if (!training.name.trim() || !training.bow.id.trim() || training.rounds.length === 0) {
      console.warn('Name, Bow ID and Rounds must not be empty');
      return;
    }
    const requestPayload = new TrainingRequestModelImpl(
      userId,
      training.name,
      new Date(),
      training.bow.id,
      training.rounds.map(round => new TrainingRoundRequestModelImpl(round.targetFace.id, round.numberOfEnds, round.arrowInEachEnd, round.distance, round.note))
    );
    TrainingService.createUserTrainings(requestPayload)
      .then(training => NavigationService.navigate('Training Rounds', { trainingId: training.id }))
      .catch(error => console.error('Error creating training-round-creation-list: ', error));
  };

  /**
   * setBow is a function that sets the selected bow in the training.
   *
   * @param {BowModel} bow - The selected bow.
   */
  const setBow = (bow: BowModel) => {
    setTraining(prevTraining => ({
      ...prevTraining,
      bow: bow
    }));
  };

  /**
   * updateTrainingRounds is a function that updates the rounds in the training.
   * If the round number already exists, it updates the existing round.
   * If the round number does not exist, it adds the new round to the rounds array.
   *
   * @param {TrainingRoundModel} trainingRound - The training round to be updated or added.
   */
  const updateTrainingRounds = (trainingRound: TrainingRoundModel) => {
    if (training.rounds.find(round => round.roundNumber === trainingRound.roundNumber)) {
      setTraining(prevTraining => ({
        ...prevTraining,
        rounds: prevTraining.rounds.map(round => round.roundNumber === trainingRound.roundNumber ? trainingRound : round)
      }));
    } else {
      setTraining(prevTraining => ({
        ...prevTraining,
        rounds: [...prevTraining.rounds, trainingRound]
      }));
    }

  };

  /**
   * navigateToNewTrainingRound is a function that navigates to the 'Training Round Creation' screen for creating a new training round.
   */
  const navigateToNewTrainingRound = () => {
    const newRoundNumber = training.rounds.length + 1;
    const existingTrainingRound = new TrainingRoundModelImpl('', newRoundNumber);
    NavigationService.navigate('Training Round Creation', { existingTrainingRound, updateTrainingRounds });
  };

  return (
    <View style={styles.view}>
      <TextInput
        label="Name"
        mode="outlined"
        activeOutlineColor={SharedStyles.darkGrey}
        placeholder="Name"
        value={training.name}
        onChangeText={text => setTraining({ ...training, name: text })}
      />
      {training.rounds.map((trainingRound, index) => (
        <TouchableOpacity key={index} onPress={() => NavigationService.navigate('Training Round Creation', {
          existingTrainingRound: trainingRound,
          updateTrainingRounds
        })}>
          <RoundComponent trainingRound={trainingRound} />
        </TouchableOpacity>
      ))}
      <TouchableOpacity onPress={() => NavigationService.navigate('Bows', { setBow })}>
        <BowComponent bow={training.bow} isSelected={!!training.bow.id.trim()} />
      </TouchableOpacity>
      <View style={styles.arrowContainer}>
        <CheckBoxInputComponent
          viewStyle={styles.checkboxWidth}
          label="Free Training"
          isSelected={isSelectedFreeTraining}
          setSelection={setIsSelectedFreeTraining}
        />
        <CheckBoxInputComponent
          viewStyle={styles.checkboxWidth}
          label="Use arrow numbers"
          isSelected={isSelectedUseArrowNumbers}
          setSelection={setIsSelectedUseArrowNumbers}
        />
        <TextInput
          label="Number of arrows"
          mode="outlined"
          activeOutlineColor={SharedStyles.darkGrey}
          placeholder="Number of arrows"
          keyboardType="numeric"
            style={styles.numbArrowInput}
        />
      </View>
      <TouchableOpacity onPress={() => console.warn('WeatherScreen is not implemented!')}>
        <WeatherComponent />
      </TouchableOpacity>
      <CheckBoxInputComponent
        viewStyle={styles.checkboxWidth}
        label="Use clocks"
        isSelected={isSelectedUseClocks}
        setSelection={setIsSelectedUseClocks}
      />
      <TouchableOpacity onPress={() => navigateToNewTrainingRound()} style={styles.addButton}>
        <Icon name="add" size={80} color={SharedStyles.black}></Icon>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  checkboxWidth: {
    flexGrow: 1,
    maxWidth: 90
  },
  arrowContainer: {
    flexDirection: 'row',
    gap: 5
  },
  view: {
    backgroundColor: SharedStyles.white,
    flexGrow: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 10
  },
  numbArrowInput: {
    flexGrow: 1
  },
  addButton: {
    backgroundColor: SharedStyles.yellow,
    borderRadius: 50,
    width: 80,
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 20,
    right: 20
  }
});

export default TrainingCreationScreen;
