import React, {useEffect, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {BowModel} from '../../models/BowModel.tsx';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import ListComponent from '../../components/list/ListComponent.tsx';
import NavigationService from '../../services/common/NavigationReferenceService.tsx';
import UserAccountService from '../../services/api/UserAccountService.tsx';

/**
 * BowListScreen is a React component that displays a list of bows.
 * The user can select a bow from the list, which will then be set in the parent component.
 *
 * @param {object} route - The route object provided by React Navigation.
 * @returns {JSX.Element} A React component.
 */
const BowListScreen = ({ route }: any): JSX.Element => {
  /**
   * bows is the current list of BowModel objects.
   * setBows is a function to update the list of BowModel objects.
   */
  const [bows, setBows] = useState<BowModel[]>([]);

  /**
   * This effect fetches the list of bows for the active user when the component is mounted.
   */
  useEffect(() => {
    UserAccountService.getActiveUserId()
      .then((userId) => {
        if (!userId) {
          throw new Error('No active user id found!');
        }
        UserAccountService.getUserBows(userId)
          .then((fetchedBows) => {
            setBows(fetchedBows);
          })
          .catch((error) => console.error(error));
      })
      .catch((error) => console.error(error));
  }, []);

  /**
   * selectBow is a function that sets the selected bow in the parent component and navigates back.
   *
   * @param {BowModel} bow - The selected bow.
   */
  const selectBow = (bow: BowModel) => {
    const { setBow } = route.params;
    setBow(bow);
    NavigationService.goBack();
  };

  return (
    <View style={styles.view}>
      <ListComponent views={bows.map((bow) => (
        <TouchableOpacity key={bow.id} style={styles.itemRow} onPress={() => selectBow(bow)}>
          <Image source={require('../../assets/images/regualar_bow.png')}
                 style={{ width: 50, height: 50, marginRight: 10 }} />
          <View style={styles.headerView}>
            <Text>{bow.name}</Text>
            <Text>{bow.bowType.name}</Text>
            <Text>{bow.strength} lbs</Text>
          </View>
          <Icon name="edit" size={40} color={SharedStyles.black}></Icon>
        </TouchableOpacity>
      ))} />
      <TouchableOpacity onPress={() => console.warn('NewBowScreen is not implemented!')} style={styles.addButton}>
        <Icon name="add" size={80} color={SharedStyles.black}></Icon>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    flexGrow: 1
  },
  itemRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    gap: 10,
    alignItems: 'center'
  },
  headerView: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1
  },
  header: {
    fontWeight: 'bold'
  },
  addButton: {
    backgroundColor: SharedStyles.yellow,
    borderRadius: 50,
    width: 80,
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 20,
    right: 20
  }
});

export default BowListScreen;
