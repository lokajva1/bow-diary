import ListComponent from '../../components/list/ListComponent.tsx';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import TrainingRoundService from '../../services/api/TrainingRoundService.tsx';
import {TrainingRoundModel, TrainingRoundModelImpl} from '../../models/TrainingRoundModel.tsx';
import {ScoreSheetModelImpl} from '../../models/ScoreSheetModel.tsx';
import React, {useEffect} from 'react';
import {useIsFocused} from '@react-navigation/native';
import NavigationService from '../../services/common/NavigationReferenceService.tsx';

/**
 * RoundEndListScreen is a React component that displays a list of round ends.
 * The user can select a round end from the list, which will then navigate to the 'Arrow Positioning' screen.
 *
 * @param {object} route - The route object provided by React Navigation.
 * @returns {JSX.Element} A React component.
 */
const RoundEndListScreen = ({ route}: any): JSX.Element => {
  /**
   * isFocused is a boolean that indicates whether the screen is currently focused.
   */
  const isFocused = useIsFocused();

  /**
   * trainingRound is the current TrainingRoundModel object that represents the training round.
   * setTrainingRound is a function to update the TrainingRoundModel object.
   */
  const [trainingRound, setTrainingRound] = React.useState<TrainingRoundModel>(new TrainingRoundModelImpl());

  /**
   * This effect fetches the active round id and the corresponding training round when the component is focused.
   * If the 'automaticallyNavigateToArrowPositioning' flag is true and there is an active round end, it navigates to the 'Arrow Positioning' screen.
   */
  useEffect(() => {
    const automaticallyNavigateToArrowPositioning = route.params.automaticallyNavigateToArrowPositioning
    TrainingRoundService.getActiveRoundId()
      .then((id) => {
        if (id === null) {
          throw new Error('No round id provided');
        }
        console.log("fetching training round with id: ", id);
        TrainingRoundService.getTrainingRound(id)
          .then((fetchedTrainingRound) => {
            setTrainingRound(fetchedTrainingRound);
            const activeRoundEnd = fetchedTrainingRound.getActiveRoundEnd();
            if (automaticallyNavigateToArrowPositioning && activeRoundEnd) {
              navigateToArrowPositioning(activeRoundEnd.id, fetchedTrainingRound);
            }
          })
          .catch((error) => console.error(error));
      })
      .catch((error) => console.error(error));
  }, [isFocused]);

  /**
   * navigateToArrowPositioning is a function that creates a new ScoreSheetModel object and navigates to the 'Arrow Positioning' screen.
   *
   * @param {string} id - The id of the round end.
   * @param {TrainingRoundModel} fetchedTrainingRound - The fetched training round.
   */
  const navigateToArrowPositioning = (id: string, fetchedTrainingRound: TrainingRoundModel = trainingRound) => {
    const roundEndIdx = fetchedTrainingRound.roundEnds.findIndex((end) => end.id === id) ;
    const roundEnd = fetchedTrainingRound.roundEnds.at(roundEndIdx);
    const scoreSheet = new ScoreSheetModelImpl(
      roundEnd?.id,
      roundEndIdx + 1,
      fetchedTrainingRound.arrowInEachEnd,
      roundEnd?.arrowPositions,
      fetchedTrainingRound.getScore(),
      fetchedTrainingRound.targetFace
    );
    NavigationService.navigate('Arrow Positioning', { scoreSheet });
  };

  return (
    <ListComponent views={trainingRound.roundEnds.map((end, idx) => (
      <TouchableOpacity key={end.id} style={styles.itemRow} onPress={() => navigateToArrowPositioning(end.id)}>
        <Icon name="adjust" size={40} color={SharedStyles.black}></Icon>
        <View style={styles.headerView}>
          <Text style={styles.header}>End {idx}</Text>
        </View>
        <View style={styles.scoreView}>
          <Text>{end.getArrowsShot()}/{trainingRound.arrowInEachEnd}</Text>
          <Text>{end.getScore()}/{trainingRound.getMaxScorePerRoundEnd()}</Text>
        </View>
        <Icon name="edit" size={40} color={SharedStyles.black}></Icon>
      </TouchableOpacity>
    ))} />
  );
};

export default RoundEndListScreen;

const styles = StyleSheet.create({
  itemRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    gap: 10,
    alignItems: 'center'
  },
  headerView: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1
  },
  header: {
    fontWeight: 'bold'
  },
  scoreView: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  note: {
    marginLeft: 5
  }
});
