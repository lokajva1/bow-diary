import React, {useEffect, useLayoutEffect, useRef} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import {TrainingModel, TrainingModelImpl} from '../../models/TrainingModel.tsx';
import ListComponent from '../../components/list/ListComponent.tsx';
import Icon from 'react-native-vector-icons/MaterialIcons';
import TrainingService from '../../services/api/TrainingService.tsx';
import TrainingRoundService from '../../services/api/TrainingRoundService.tsx';
import {useIsFocused} from '@react-navigation/native';
import NavigationService from '../../services/common/NavigationReferenceService.tsx';

/**
 * TrainingRoundListScreen is a React component that displays a list of training rounds.
 * The user can select a training round from the list, which will then navigate to the 'Training Round Ends' screen.
 *
 * @param {object} navigation - The navigation object provided by React Navigation.
 * @param {object} route - The route object provided by React Navigation.
 * @returns {JSX.Element} A React component.
 */
const TrainingRoundListScreen = ({ navigation, route }: any) => {
  /**
   * isFocused is a boolean that indicates whether the screen is currently focused.
   */
  const isFocused = useIsFocused();

  /**
   * trainingId is the id of the current training.
   */
  const { trainingId } = route.params as { trainingId: string };

  /**
   * training is the current TrainingModel object that represents the training.
   * setTraining is a function to update the TrainingModel object.
   */
  const [training, setTraining] = React.useState<TrainingModel>(new TrainingModelImpl());

  /**
   * trainingRef is a reference to the current TrainingModel object.
   */
  const trainingRef = useRef<TrainingModel>(new TrainingModelImpl());

  /**
   * This effect fetches the training with the given id when the component is focused.
   * If the training is successfully fetched, it updates the training state and the trainingRef reference.
   */
  useEffect(() => {
    console.log('fetching training with id: ', trainingId);
    TrainingService.getTraining(trainingId)
      .then((fetchedTraining) => {
        setTraining(fetchedTraining);
        trainingRef.current = fetchedTraining;
      })
      .catch((error) => console.error(error));
  }, [isFocused]);

  /**
   * This effect sets the header left button to be an empty view and the header right button to be a 'Done' icon.
   * When the 'Done' icon is clicked, it navigates to the 'My Trainings' screen.
   */
  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <View />
      ),
      headerRight: () => (
        <TouchableOpacity style={{ flexDirection: 'row', gap: 10 }}>
          <Icon name="analytics" size={30} color={SharedStyles.white}
                onPress={() => {
                  NavigationService.navigate('Training Statistic', { training: trainingRef.current })}} />
          <Icon name="done" size={30} color={SharedStyles.white}
                onPress={() => NavigationService.navigate('My Trainings')} />
        </TouchableOpacity>
      )
    });
  }, [navigation]);

  /**
   * navigateToTrainingRoundScreen is a function that sets the active round id and navigates to the 'Training Round Ends' screen.
   *
   * @param {string} id - The id of the round.
   */
  const navigateToTrainingRoundScreen = (id: string) => {
    TrainingRoundService.setActiveRoundId(id)
      .then(() => NavigationService.navigate('Training Round Ends', { automaticallyNavigateToArrowPositioning: true }));
  };

  return (
    <ListComponent views={training.rounds.map((round) => (
      <View style={styles.container}>
        <TouchableOpacity key={round.id} style={styles.itemRow} onPress={() => navigateToTrainingRoundScreen(round.id)}>
          <Icon name="adjust" size={40} color={SharedStyles.black}></Icon>
          <View style={styles.headerView}>
            <Text style={styles.header}>Round {round.roundNumber}</Text>
            <Text style={styles.header}>{round.distance} m</Text>
          </View>
          <View style={styles.scoreView}>
            <Text>{round.getActiveRoundEndIdx()}/{round.numberOfEnds}</Text>
            <Text>{round.getScore()}/{round.getMaxScore()}</Text>
          </View>
          <Icon name="edit" size={40} color={SharedStyles.black}></Icon>
        </TouchableOpacity>
        {round.note.length > 0 &&
          <Text style={styles.note}><Text style={styles.header}>Note:</Text> {round.note}</Text>}
      </View>
    ))} />
  );
};

export default TrainingRoundListScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  itemRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    gap: 10,
    alignItems: 'center'
  },
  headerView: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1
  },
  header: {
    fontWeight: 'bold'
  },
  scoreView: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  note: {
    marginLeft: 5
  }
});
