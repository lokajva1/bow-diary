import {useEffect, useLayoutEffect, useState} from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {
  TrainingRoundModel,
} from '../../models/TrainingRoundModel.tsx';
import TargetFaceComponent from '../../components/training-round/TargetFaceComponent.tsx';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import {TextInput} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {NumberService} from '../../services/common/NumberService.tsx';
import TargetFaceService from '../../services/api/TargetFaceService.tsx';
import NavigationService from '../../services/common/NavigationReferenceService.tsx';

/**
 * TrainingRoundCreationScreen is a React component that provides an interface for creating a new training round.
 *
 * @param {object} navigation - The navigation object provided by React Navigation.
 * @param {object} route - The route object provided by React Navigation.
 * @returns {JSX.Element} A React component.
 */
const TrainingRoundCreationScreen = ({ navigation, route }: any) => {
  /**
   * existingTrainingRound is the current TrainingRoundModel object that represents the existing training round.
   * updateTrainingRounds is a function to update the TrainingRoundModel object.
   */
  const { existingTrainingRound } = route.params;
  const { updateTrainingRounds } = route.params;

  /**
   * isCreateTrainingRoundClicked is a boolean that indicates whether the 'Create Training Round' button has been clicked.
   * setIsCreateTrainingClicked is a function to update the isCreateTrainingRoundClicked state.
   */
  const [isCreateTrainingRoundClicked, setIsCreateTrainingClicked] = useState(false);

  /**
   * trainingRound is the current TrainingRoundModel object that represents the training round being created.
   * setTrainingRound is a function to update the TrainingRoundModel object.
   */
  const [trainingRound, setTrainingRound] = useState<TrainingRoundModel>(existingTrainingRound);

  /**
   * This effect fetches all target faces when the component is mounted.
   * If there are target faces, it sets the first target face in the training round.
   * If the 'Create Training Round' button has been clicked, it calls the saveTrainingRound function.
   */
  useEffect(() => {
    TargetFaceService.getAllTargetFaces()
      .then(targetFaceModels => {
        if (targetFaceModels.length){
          setTrainingRound(prevState => ({
            ...prevState,
            targetFace: targetFaceModels[0]
          }))
        }
      })
    if (isCreateTrainingRoundClicked) {
      setIsCreateTrainingClicked(false);
      saveTrainingRound();
    }
  }, [isCreateTrainingRoundClicked]);

  /**
   * This effect sets the header right button to be the 'Done' icon.
   * When the 'Done' icon is clicked, it sets the isCreateTrainingRoundClicked state to true.
   */
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => {
        return <Icon name="done" size={30} color={SharedStyles.white} onPress={() => setIsCreateTrainingClicked(true)} />;
      }
    });
  }, [navigation]);

  /**
   * saveTrainingRound is a function that saves the training round.
   * It first checks if the number of ends, arrows in each end, and distance are greater than 0.
   * If they are, it calls the updateTrainingRounds function with the training round and navigates back.
   */
  const saveTrainingRound = () => {
    if (trainingRound.numberOfEnds <= 0 || trainingRound.arrowInEachEnd <= 0 || trainingRound.distance <= 0) {
      console.warn('Number of ends, arrows in each end and distance must be greater than 0');
      return;
    }

    updateTrainingRounds(trainingRound);
    NavigationService.goBack();
  };

  return (
    <View style={styles.view}>
      <TouchableOpacity onPress={() => console.warn('TargetFaceScreen is not implemented!')}>
        <TargetFaceComponent targetFace={trainingRound.targetFace} />
      </TouchableOpacity>
      <View style={styles.rowComponent}>
        <TextInput
          label="Number of ends"
          placeholder="Number of ends"
          mode="outlined"
          keyboardType="numeric"
          style={styles.rowTextInput}
          activeOutlineColor={SharedStyles.darkGrey}
          value={trainingRound.numberOfEnds.toString()}
          onChangeText={text => setTrainingRound({
            ...trainingRound,
            numberOfEnds: NumberService.parseIntInput(text)
          })} />
        <TextInput
          label="Arrows in each end"
          placeholder="Arrows in each end"
          mode="outlined"
          keyboardType="numeric"
          style={styles.rowTextInput}
          activeOutlineColor={SharedStyles.darkGrey}
          value={trainingRound.arrowInEachEnd.toString()}
          onChangeText={text => setTrainingRound({
            ...trainingRound,
            arrowInEachEnd: NumberService.parseIntInput(text)
          })} />
      </View>
      <TextInput
        label="Distance"
        placeholder="Distance"
        mode="outlined"
        keyboardType="numeric"
        activeOutlineColor={SharedStyles.darkGrey}
        value={trainingRound.distance.toString()}
        onChangeText={text => setTrainingRound({ ...trainingRound, distance: NumberService.parseIntInput(text) })} />
      <TextInput
        label="Note"
        placeholder="Note"
        mode="outlined"
        style={styles.note}
        activeOutlineColor={SharedStyles.darkGrey}
        value={trainingRound.note.toString()}
        onChangeText={text => setTrainingRound({ ...trainingRound, note: text })} />
    </View>
  );
};

const styles = StyleSheet.create({
  view: {
    backgroundColor: SharedStyles.white,
    flexGrow: 1,
    paddingLeft: 20,
    paddingRight: 20,
    paddingTop: 5,
    paddingBottom: 10
  },
  rowComponent: {
    flexDirection: 'row',
    gap: 10
  },
  rowTextInput: {
    flexGrow: 1
  },
  note: {
    flexGrow: 1
  }
});

export default TrainingRoundCreationScreen;
