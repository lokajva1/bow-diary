import React, {useLayoutEffect, useRef, useState} from 'react';
import {Animated, Dimensions, Image, PanResponder, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import {ArrowPositionImpl} from '../../models/ArrowPosition.tsx';
import {ScoreSheetModel} from '../../models/ScoreSheetModel.tsx';
import ScoreSheetComponent from '../../components/training/ScoreSheetComponent.tsx';
import RoundEndService from '../../services/api/RoundEndService.tsx';
import {RoundEndImpl} from '../../models/RoundEnd.tsx';
import Icon from 'react-native-vector-icons/MaterialIcons';
import NavigationService from '../../services/common/NavigationReferenceService.tsx';

/**
 * Destructures the 'window' object from the Dimensions API in React Native.
 * This provides the width and height of the window of the device.
 *
 * @constant
 * @type {object}
 * @property {number} width - The width of the window of the device.
 * @property {number} height - The height of the window of the device.
 */
const { width, height } = Dimensions.get('window');

/**
 * ArrowPositioningScreen is a React component that allows the user to position arrows on a target face.
 * The user can add, remove, and save the positions of the arrows.
 *
 * @param {object} navigation - The navigation object provided by React Navigation.
 * @param {object} route - The route object provided by React Navigation.
 * @returns {JSX.Element} A React component.
 */
const ArrowPositioningScreen = ({ navigation, route}: any): JSX.Element => {
  /**
   * pan is a reference to an Animated.ValueXY object that controls the position of the arrow on the target face.
   */
  const pan = useRef(new Animated.ValueXY()).current;

  /**
   * scoreSheet is the current score sheet model.
   * setScoreSheet is a function to update the score sheet model.
   */
  const [scoreSheet, setScoreSheet] = useState(route.params.scoreSheet as ScoreSheetModel);

  /**
   * arrowScoreLabel is the current score label of the arrow.
   * setArrowScoreLabel is a function to update the score label of the arrow.
   */
  const [arrowScoreLabel, setArrowScoreLabel] = useState('X');

  /**
   * This effect changes the behaviour of the go back button in the header.
   */
  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          onPress={() => saveRound(false)}>
          <Icon
            name="arrow-back"
            size={24}
            color={SharedStyles.white}
            style={styles.arrowBack}
          />
        </TouchableOpacity>
      )
    });
  }, [navigation]);

  /**
   * panResponder is a PanResponder instance that handles the user's touch events on the target face.
   */
  const panResponder = useRef(
    PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => true,
      onPanResponderGrant: () => {
        pan.setOffset({
          // @ts-ignore
          x: pan.x._value,
          // @ts-ignore
          y: pan.y._value
        });
      },
      onPanResponderMove: (e, gestureState) => {
        pan.x.setValue(gestureState.dx);
        pan.y.setValue(gestureState.dy);
        setArrowScoreLabel(calculateArrowScore());
      },
      onPanResponderRelease: () => {
        pan.flattenOffset();
      }
    })
  ).current;

  /**
   * calculateArrowScore calculates the score of the arrow based on its position on the target face.
   *
   * @returns {string} The score of the arrow.
   */
  const calculateArrowScore = (): string => {
    const layout = pan.getLayout();
    // @ts-ignore
    const absoluteX = layout.left.__getValue();
    // @ts-ignore
    const absoluteY = layout.top.__getValue();
    const distance = Math.sqrt(absoluteX * absoluteX + absoluteY * absoluteY);
    const ring = scoreSheet.targetFace.ringsDistance.findIndex((ringDistance) => distance < ringDistance);
    return ring === 0 ? 'X' : (ring === -1 ? 'M' : (11 - ring).toString());
  };

  /**
   * popArrow removes the last arrow from the score sheet model.
   */
  const popArrow = () => {
    setScoreSheet(prevScoreSheet => {
      prevScoreSheet.arrowPositions.pop();
      return scoreSheet.copy(prevScoreSheet);
    });
    resetDot();
  };

  /**
   * addArrow adds a new arrow to the score sheet model based on the current position of the dot on the target face.
   */
  const addArrow = () => {
    const layout = pan.getLayout();
    // @ts-ignore
    const absoluteX = layout.left.__getValue();
    // @ts-ignore
    const absoluteY = layout.top.__getValue();
    // Add the current position of the dot to the marks array
    setScoreSheet(prevScoreSheet => {
      prevScoreSheet.arrowPositions.push(new ArrowPositionImpl('', absoluteX, absoluteY, arrowScoreLabel));
      return scoreSheet.copy(prevScoreSheet);
    });
    resetDot();
  };

  /**
   * resetDot resets the position of the dot on the target face to the center.
   */
  const resetDot = () => {
    pan.setValue({ x: 0, y: 0 });
    setArrowScoreLabel('X');
  };

  /**
   * saveRound saves the current round and navigates to the 'Training Round Ends' screen.
   *
   * @param {boolean} automaticallyNavigateToArrowPositioning - A flag that indicates whether to automatically navigate to the Arrow Positioning screen.
   */
  const saveRound = (automaticallyNavigateToArrowPositioning: boolean) => {
    const roundEnd = new RoundEndImpl(scoreSheet.roundEndId, scoreSheet.arrowPositions);
    RoundEndService.updateRoundEnd(roundEnd)
      .then(() => {
        NavigationService.navigate('Training Round Ends', { automaticallyNavigateToArrowPositioning });
      })
      .catch((error) => {
        console.error('Error saving round end: ', error);
      });
  };

  return (
    <View style={styles.container}>
      <Animated.View
        style={[
          { transform: pan.getTranslateTransform() }
        ]}
        {...panResponder.panHandlers}
      >
        {scoreSheet.arrowPositions.map((mark, index) => (
          <View
            key={index}
            style={[
              styles.mark,
              {
                left: width / 2 - mark.x,
                top: height / 2 - mark.y
              }
            ]}
          />
        ))}

        <Image
          source={require('../../assets/images/wa_80_target_face.png')}
          style={styles.image}
        />
      </Animated.View>
      <View style={[styles.dot, styles.centerDot]} />
      <Animated.Text style={styles.distanceText}>
        {arrowScoreLabel}
      </Animated.Text>
      <View style={styles.scoreSheetContainer}>
        <ScoreSheetComponent scoreSheet={scoreSheet}></ScoreSheetComponent>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} disabled={scoreSheet.arrowPositions.length == 0} onPress={popArrow}>
          <Text
            style={[styles.buttonText, scoreSheet.arrowPositions.length == 0 && styles.buttonTextDisabled]}>REMOVE</Text>
        </TouchableOpacity>
        {
          scoreSheet.arrowPositions.length < scoreSheet.maxArrows &&
          <TouchableOpacity style={styles.button} onPress={addArrow}>
            <Text style={styles.buttonText}>ADD</Text>
          </TouchableOpacity>
          ||
          <TouchableOpacity style={styles.button} onPress={() => saveRound(true)}>
            <Text style={styles.buttonText}>NEXT</Text>
          </TouchableOpacity>
        }
      </View>
    </View>
  );
};

export default ArrowPositioningScreen;

const styles = StyleSheet.create({
  arrowBack: {
    marginRight: 7,
    marginTop: 1,
    marginLeft: 0
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: SharedStyles.white
  },
  image: {
    width: width,
    height: height,
    resizeMode: 'contain'
  },
  mark: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: SharedStyles.black,
    borderWidth: 1,
    borderColor: SharedStyles.grey,
    transform: [{ translateX: -5 }, { translateY: -5 }],
    zIndex: 1,
    position: 'absolute'
  },
  overlayView: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: 50, // or any size you want
    height: 50, // or any size you want
    backgroundColor: 'red' // or any color you want
  },
  dot: {
    width: 10,
    height: 10,
    borderRadius: 5,
    backgroundColor: SharedStyles.black,
    borderWidth: 1,
    borderColor: SharedStyles.grey,
    transform: [{ translateX: -5 }, { translateY: -5 }]
  },
  centerDot: {
    top: '50%',
    left: '50%',
    position: 'absolute'
  },
  distanceText: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: [{ translateX: 0 }, { translateY: -60 }],
    fontSize: 48,
    color: SharedStyles.black
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    bottom: 0,
    width: '100%'
  },
  button: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
    borderWidth: 1
  },
  buttonText: {
    fontSize: 32,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center'
  },
  scoreSheetContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '100%'
  },
  buttonTextDisabled: {
    color: SharedStyles.grey
  }
});
