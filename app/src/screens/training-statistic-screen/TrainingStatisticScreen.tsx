import React, {useLayoutEffect} from 'react';
// @ts-ignore
import {BarChart, Grid, XAxis} from 'react-native-svg-charts';
import {Text, TouchableOpacity, View} from 'react-native';
import BarChartComponent from '../../components/training-statistic/BarChartComponent.tsx';
import {TrainingModel} from '../../models/TrainingModel.tsx';
import SharedStyles from '../../assets/style/SharedStyles.tsx';
import LineChartComponent from '../../components/training-statistic/LineChartComponent.tsx';
import Icon from 'react-native-vector-icons/MaterialIcons';

/**
 * TrainingStatisticScreen is a React component that displays the statistics of a training.
 *
 * @param {object} navigation - The navigation object provided by React Navigation.
 * @param {object} route - The route object provided by React Navigation.
 * @returns {JSX.Element} A React component.
 */
const TrainingStatisticScreen = ({ navigation, route }: any) => {
  /**
   * training is the current TrainingModel object that represents the training.
   */
  const { training } = route.params as { training: TrainingModel };

  /**
   * distributionHeaders is an array of strings that represents the headers for the distribution of arrows over the target face.
   */
  const distributionHeaders = ['M', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'X'];

  /**
   * This effect sets the header right button to be a 'Share' icon.
   * When the 'Share' icon is clicked, it warns that the 'ShareScreen' is not implemented.
   */
  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity style={{ flexDirection: 'row', gap: 10 }}>
          <Icon name="share" size={30} color={SharedStyles.white}
                onPress={() => console.warn("ShareScreen not implemented!")} />
        </TouchableOpacity>
      )
    });
  }, [navigation]);

  /**
   * calcDistributionOfArrows is a function that calculates the distribution of arrows over the target face.
   * It creates a new array with 11 elements, all initialized to 0.
   * It then increments the corresponding element in the array for each arrow's score.
   *
   * @param {TrainingModel} training - The training to calculate the distribution of arrows for.
   * @returns {number[]} An array of numbers that represents the distribution of arrows over the target face.
   */
  const calcDistributionOfArrows = (training: TrainingModel) => {
    const distribution = new Array(11).fill(0);
    training.rounds.flatMap(round => round.roundEnds).forEach(end => {
      end.arrowPositions.forEach(arrow => distribution[arrow.getScore()]++);
    });
    return distribution;
  };

  /**
   * calcDistributionOfRoundEndScore is a function that calculates the distribution of round end scores.
   * It maps each round end to its score and flattens the result into a single array.
   *
   * @param {TrainingModel} training - The training to calculate the distribution of round end scores for.
   * @returns {number[]} An array of numbers that represents the distribution of round end scores.
   */
  const calcDistributionOfRoundEndScore = (training: TrainingModel) => {
    return training.rounds.flatMap(round =>
      round.roundEnds.map(end => end.getScore())
    );
  };

  /**
   * makeLineHeaders is a function that creates the headers for the line chart.
   * It maps each round end to a string in the format 'roundIdx/endIdx' and flattens the result into a single array.
   *
   * @param {TrainingModel} training - The training to create the line headers for.
   * @returns {string[]} An array of strings that represents the headers for the line chart.
   */
  const makeLineHeaders = (training: TrainingModel) => {
    return training.rounds.flatMap((round, roundIdx) =>
      round.roundEnds.map((end, endIdx) => `${roundIdx + 1}/${endIdx + 1}`)
    );
  };

  return (
    <View style={{ flexDirection: 'column', flexGrow: 1, padding: 10, backgroundColor: SharedStyles.white }}>
      <View style={{ height: '50%', paddingBottom: 10 }}>
        <Text style={{ fontSize: 16, color: SharedStyles.black, fontWeight: 'bold' }}>Arrows over target face distribution</Text>
        <BarChartComponent data={calcDistributionOfArrows(training)} headers={distributionHeaders} />
      </View>
      <View style={{ height: '50%', paddingTop: 10  }}>
        <Text style={{ fontSize: 16, color: SharedStyles.black, fontWeight: 'bold' }}>Score per round end</Text>
        <LineChartComponent data={calcDistributionOfRoundEndScore(training)} headers={makeLineHeaders(training)} />
      </View>
    </View>
  );
};

export default TrainingStatisticScreen;
