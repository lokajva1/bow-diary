import {RoundEnd} from '../../models/RoundEnd.tsx';
import AbstractApiService from './AbstractApiService.tsx';

/**
 * RoundEndService is an object that extends AbstractApiService.
 * It includes a method for updating a round end.
 *
 * @property {function} updateRoundEnd - A method that updates a round end.
 */
const RoundEndService = {
  ...AbstractApiService,

  /**
   * updateRoundEnd is a method that updates a round end.
   * It makes a PUT request to the '/round-end/{id}' endpoint with the round end as the request body.
   *
   * @param {RoundEnd} roundEnd - The round end to update.
   * @returns {Promise<void>} A promise that resolves to undefined.
   */
  async updateRoundEnd(roundEnd: RoundEnd): Promise<void> {
    return this.put(`/round-end/${roundEnd.id}`, roundEnd);
  },
}

export default RoundEndService;
