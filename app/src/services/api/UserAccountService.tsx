import AbstractApiService from './AbstractApiService.tsx';
import {UserAccountModel} from '../../models/UserAccountModel.tsx';
import AsyncStorage from '@react-native-async-storage/async-storage';
import TrainingService from './TrainingService.tsx';
import {BowModel} from '../../models/BowModel.tsx';

/**
 * UserAccountService is an object that extends AbstractApiService.
 * It includes methods for generating a token, getting user trainings, getting user bows, setting the active user id, and getting the active user id.
 *
 * @property {function} generateToken - A method that generates a token.
 * @property {function} getUserTrainings - A method that gets user trainings.
 * @property {function} getUserBows - A method that gets user bows.
 * @property {function} setActiveUserId - A method that sets the active user id.
 * @property {function} getActiveUserId - A method that gets the active user id.
 */
const UserAccountService = {
  ...AbstractApiService,

  /**
   * generateToken is a method that generates a token.
   * It makes a POST request to the '/user/login' endpoint with the user account as the request body.
   *
   * @param {UserAccountModel} userAccount - The user account to generate a token for.
   * @returns {Promise<any>} A promise that resolves to the response data.
   */
  async generateToken(userAccount: UserAccountModel) {
    return this.post('/user/login', userAccount);
  },

  /**
   * getUserTrainings is a method that gets user trainings.
   * It makes a GET request to the '/user/{userId}/trainings' endpoint.
   * It then maps the training data to a TrainingModel using the mapTrainingDataToTrainingModel method from TrainingService.
   *
   * @param {string} userId - The id of the user to get trainings for.
   * @returns {Promise<TrainingModel[]>} A promise that resolves to an array of TrainingModel objects.
   */
  async getUserTrainings(userId: string) {
    const trainings = await this.get(`/user/${userId}/trainings`);
    return trainings.map(TrainingService.mapTrainingDataToTrainingModel);
  },

  /**
   * getUserBows is a method that gets user bows.
   * It makes a GET request to the '/user/{userId}/bows' endpoint.
   *
   * @param {string} userId - The id of the user to get bows for.
   * @returns {Promise<BowModel[]>} A promise that resolves to an array of BowModel objects.
   */
  async getUserBows(userId: string): Promise<BowModel[]> {
    return this.get(`/user/${userId}/bows`);
  },

  /**
   * setActiveUserId is a method that sets the active user id.
   * It stores the id in AsyncStorage under the key 'activeUserId'.
   *
   * @param {string} id - The id to set as the active user id.
   * @returns {Promise<void>} A promise that resolves to undefined.
   */
  async setActiveUserId(id: string): Promise<void> {
    await AsyncStorage.setItem('activeUserId', id);
  },

  /**
   * getActiveUserId is a method that gets the active user id.
   * It retrieves the id from AsyncStorage under the key 'activeUserId'.
   *
   * @returns {Promise<string | null>} A promise that resolves to the active user id or null if it does not exist.
   */
  async getActiveUserId(): Promise<string | null> {
    return AsyncStorage.getItem('activeUserId');
  }
};

export default UserAccountService;
