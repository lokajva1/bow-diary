import axios, {HttpStatusCode} from 'axios';
// @ts-ignore - works fine
import {NO_TOKEN_ENPOINTS, REACT_APP_BACKEND_URL} from '@env';
import TokenService from '../common/TokenService.tsx';
import NavigationService from '../common/NavigationReferenceService.tsx';

/**
 * AbstractApiService is an object that provides methods for making HTTP requests.
 * It includes methods for GET, POST, and PUT requests.
 * It also includes a method for getting the backend URL.
 *
 * @property {string} backendUrl - The backend URL.
 * @property {string[]} noTokenEndpoints - The endpoints that do not require a token.
 */
const AbstractApiService = {
  backendUrl: '',
  noTokenEndpoints: NO_TOKEN_ENPOINTS.split(','),

  /**
   * getBackendUrl is a method that returns the backend URL.
   * If the backend URL is not set, it throws an error.
   *
   * @returns {string} The backend URL.
   */
  getBackendUrl(): string {
    if (!this.backendUrl) {
      const tempUrl = REACT_APP_BACKEND_URL;
      if (!tempUrl) {
        throw new Error('Backend URL not set');
      }
      this.backendUrl = tempUrl;
    }
    return this.backendUrl;
  },

  /**
   * get is a method that makes a GET request to the specified endpoint.
   * It includes the token in the Authorization header.
   * If the response status is 401 (Unauthorized), it navigates to the 'Login' screen.
   *
   * @param {string} endpoint - The endpoint to make the GET request to.
   * @returns {Promise<any>} The response data.
   * @throws {any} The error if the request fails.
   */
  get: async function(endpoint: string) {
    try {
      const token = await TokenService.getToken();
      const response = await axios.get(`${this.getBackendUrl()}${endpoint}`, {
        headers: { Authorization: `Bearer ${token}` }
      });
      return response.data;
    } catch (error: any) {
      if (error.response && error.response.status === HttpStatusCode.Unauthorized) {
        NavigationService.navigate('Login');
      }
      throw error;
    }
  },

  /**
   * post is a method that makes a POST request to the specified endpoint with the specified data.
   * If the endpoint is in the noTokenEndpoints array, it does not include the token in the Authorization header.
   * Otherwise, it includes the token in the Authorization header.
   * If the response status is 401 (Unauthorized), it navigates to the 'Login' screen.
   *
   * @param {string} endpoint - The endpoint to make the POST request to.
   * @param {any} data - The data to include in the POST request.
   * @returns {Promise<any>} The response data.
   * @throws {any} The error if the request fails.
   */
  post: async function(endpoint: string, data: any) {
    try {
      if (this.noTokenEndpoints.includes(endpoint)) {
        const response = await axios.post(`${this.getBackendUrl()}${endpoint}`, data);
        return response.data;
      }
      const token = await TokenService.getToken();
      const response = await axios.post(`${this.getBackendUrl()}${endpoint}`, data, {
        headers: { Authorization: `Bearer ${token}` }
      });
      return response.data;
    } catch (error: any) {
      if (error.response && error.response.status === HttpStatusCode.Unauthorized) {
        NavigationService.navigate('Login');
      }
      throw error;
    }
  },

  /**
   * put is a method that makes a PUT request to the specified endpoint with the specified data.
   * It includes the token in the Authorization header.
   * If the response status is 401 (Unauthorized), it navigates to the 'Login' screen.
   *
   * @param {string} endpoint - The endpoint to make the PUT request to.
   * @param {any} data - The data to include in the PUT request.
   * @returns {Promise<any>} The response data.
   * @throws {any} The error if the request fails.
   */
  put: async function(endpoint: string, data: any) {
    try {
      const token = await TokenService.getToken();
      const response = await axios.put(`${this.getBackendUrl()}${endpoint}`, data, {
        headers: { Authorization: `Bearer ${token}` }
      });
      return response.data;
    } catch (error: any) {
      if (error.response && error.response.status === HttpStatusCode.Unauthorized) {
        NavigationService.navigate('Login');
      }
      throw error;
    }
  }
};

export default AbstractApiService;
