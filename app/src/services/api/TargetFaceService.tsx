import {TargetFaceModel} from '../../models/TargetFaceModel.tsx';
import AbstractApiService from './AbstractApiService.tsx';

/**
 * TargetFaceService is an object that extends AbstractApiService.
 * It includes a method for getting all target faces.
 *
 * @property {function} getAllTargetFaces - A method that gets all target faces.
 */
const TargetFaceService = {
  ...AbstractApiService,

  /**
   * getAllTargetFaces is a method that gets all target faces.
   * It makes a GET request to the '/target-face' endpoint.
   *
   * @returns {Promise<TargetFaceModel[]>} A promise that resolves to an array of TargetFaceModel objects.
   */
  async getAllTargetFaces(): Promise<TargetFaceModel[]> {
    return this.get('/target-face');
  },
}

export default TargetFaceService;
