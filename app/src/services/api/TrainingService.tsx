import {TrainingModel, TrainingModelImpl, TrainingRequestModel} from '../../models/TrainingModel.tsx';
import {TrainingRoundModelImpl} from '../../models/TrainingRoundModel.tsx';
import {RoundEndImpl} from '../../models/RoundEnd.tsx';
import {ArrowPositionImpl} from '../../models/ArrowPosition.tsx';
import AbstractApiService from './AbstractApiService.tsx';

/**
 * TrainingService is an object that extends AbstractApiService.
 * It includes methods for getting a training, creating user trainings, and mapping training data to a TrainingModel.
 *
 * @property {function} getTraining - A method that gets a training.
 * @property {function} createUserTrainings - A method that creates user trainings.
 * @property {function} mapTrainingDataToTrainingModel - A method that maps training data to a TrainingModel.
 */
const TrainingService = {
  ...AbstractApiService,

  /**
   * getTraining is a method that gets a training.
   * It makes a GET request to the '/training/{id}' endpoint.
   * It then maps the training data to a TrainingModel using the mapTrainingDataToTrainingModel method.
   *
   * @param {string} id - The id of the training to get.
   * @returns {Promise<TrainingModel>} A promise that resolves to a TrainingModel object.
   */
  async getTraining(id: string): Promise<TrainingModel> {
    const trainingData = await this.get(`/training/${id}`);

    // Map the date string to a Date object
    return this.mapTrainingDataToTrainingModel(trainingData);
  },

  /**
   * createUserTrainings is a method that creates user trainings.
   * It makes a POST request to the '/training' endpoint with the training request as the request body.
   * It then maps the training data to a TrainingModel using the mapTrainingDataToTrainingModel method.
   *
   * @param {TrainingRequestModel} trainingRequest - The training request to create user trainings.
   * @returns {Promise<TrainingModel>} A promise that resolves to a TrainingModel object.
   */
  async createUserTrainings(trainingRequest: TrainingRequestModel): Promise<TrainingModel> {
    const trainingData = await this.post('/training', trainingRequest);

    console.log("trainingData", trainingData);
    // Map the date string to a Date object
    return this.mapTrainingDataToTrainingModel(trainingData);
  },

  /**
   * mapTrainingDataToTrainingModel is a method that maps training data to a TrainingModel.
   * It maps the rounds to instances of TrainingRoundModelImpl.
   * It then creates a new TrainingModelImpl object with the mapped rounds and the other training data.
   *
   * @param {TrainingModel} trainingData - The training data to map.
   * @returns {TrainingModel} A TrainingModel object.
   */
  mapTrainingDataToTrainingModel(trainingData: TrainingModel): TrainingModel {
    // Map the rounds to instances of TrainingRoundModelImpl
    const newTrainingRounds = trainingData.rounds.map(
      round => new TrainingRoundModelImpl(
        round.id,
        round.roundNumber,
        round.roundEnds.map(roundEnd => new RoundEndImpl(
          roundEnd.id,
          roundEnd.arrowPositions.map(arrowPosition => new ArrowPositionImpl(
            arrowPosition.id,
            arrowPosition.x,
            arrowPosition.y,
            arrowPosition.scoreLabel
          ))
        )),
        round.targetFace,
        round.numberOfEnds,
        round.arrowInEachEnd,
        round.distance,
        round.note
      )
    );
    return new TrainingModelImpl(
      trainingData.id,
      trainingData.name,
      newTrainingRounds,
      trainingData.bow,
      new Date(trainingData.date)
    );
  }
};

export default TrainingService;
