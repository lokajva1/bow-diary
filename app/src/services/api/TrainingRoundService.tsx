import {TrainingRoundModel, TrainingRoundModelImpl} from '../../models/TrainingRoundModel.tsx';
import {RoundEnd, RoundEndImpl} from '../../models/RoundEnd.tsx';
import {ArrowPositionImpl} from '../../models/ArrowPosition.tsx';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AbstractApiService from './AbstractApiService.tsx';

/**
 * TrainingRoundService is an object that extends AbstractApiService.
 * It includes methods for getting a training round, setting the active round id, and getting the active round id.
 *
 * @property {function} getTrainingRound - A method that gets a training round.
 * @property {function} setActiveRoundId - A method that sets the active round id.
 * @property {function} getActiveRoundId - A method that gets the active round id.
 */
const TrainingRoundService = {
  ...AbstractApiService,

  /**
   * getTrainingRound is a method that gets a training round.
   * It makes a GET request to the '/training-round/{id}' endpoint.
   * It then creates a new TrainingRoundModelImpl object with the response data.
   *
   * @param {string} id - The id of the training round to get.
   * @returns {Promise<TrainingRoundModel>} A promise that resolves to a TrainingRoundModel object.
   */
  async getTrainingRound(id: string): Promise<TrainingRoundModel> {
    const roundData = await this.get(`/training-round/${id}`);

    return new TrainingRoundModelImpl(
      roundData.id,
      roundData.roundNumber,
      roundData.roundEnds.map((roundEnd: RoundEnd) => new RoundEndImpl(
        roundEnd.id,
        roundEnd.arrowPositions.map(arrowPosition => new ArrowPositionImpl(
          arrowPosition.id,
          arrowPosition.x,
          arrowPosition.y,
          arrowPosition.scoreLabel,
        ))
      )),
      roundData.targetFace,
      roundData.numberOfEnds,
      roundData.arrowInEachEnd,
      roundData.distance,
      roundData.note
    )
  },

  /**
   * setActiveRoundId is a method that sets the active round id.
   * It stores the id in AsyncStorage under the key 'activeRoundId'.
   *
   * @param {string} id - The id to set as the active round id.
   * @returns {Promise<void>} A promise that resolves to undefined.
   */
  async setActiveRoundId(id: string): Promise<void> {
    await AsyncStorage.setItem('activeRoundId', id);
  },

  /**
   * getActiveRoundId is a method that gets the active round id.
   * It retrieves the id from AsyncStorage under the key 'activeRoundId'.
   *
   * @returns {Promise<string | null>} A promise that resolves to the active round id or null if it does not exist.
   */
  async getActiveRoundId(): Promise<string | null> {
    return AsyncStorage.getItem('activeRoundId');
  },
}

export default TrainingRoundService;
