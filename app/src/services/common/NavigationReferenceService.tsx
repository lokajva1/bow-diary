import { createRef, RefObject } from 'react';
import { NavigationContainerRef } from '@react-navigation/native';

/**
 * navigationRef is a reference object for the NavigationContainer.
 * It is created using React's createRef method.
 *
 * @type {RefObject<NavigationContainerRef<any>>}
 */
export const navigationRef: RefObject<NavigationContainerRef<any>> = createRef<NavigationContainerRef<any>>();

/**
 * NavigationService is an object that includes methods for navigating and going back in the navigation stack.
 *
 * @property {function} navigate - A method that navigates to a specified screen.
 * @property {function} goBack - A method that goes back in the navigation stack.
 */
const NavigationService = {
  /**
   * navigate is a method that navigates to a specified screen.
   * It uses the navigate method of the current NavigationContainer referenced by navigationRef.
   *
   * @param {string} name - The name of the screen to navigate to.
   * @param {object} [params] - The parameters to pass to the screen.
   */
  navigate(name: string, params?: object): void {
    navigationRef.current?.navigate(name, params);
  },

  /**
   * goBack is a method that goes back in the navigation stack.
   * It uses the goBack method of the current NavigationContainer referenced by navigationRef.
   */
  goBack(): void {
    navigationRef.current?.goBack();
  }
}

export default NavigationService;

