/**
 * NumberService is an object that includes a method for parsing an integer from a string.
 *
 * @property {function} parseIntInput - A method that parses an integer from a string.
 */
export const NumberService = {
  /**
   * parseIntInput is a method that parses an integer from a string.
   * If the string is empty, it returns 0.
   *
   * @param {string} text - The string to parse an integer from.
   * @returns {number} The parsed integer or 0 if the string is empty.
   */
  parseIntInput(text: string): number {
    return text ? parseInt(text) : 0;
  }
};
