/**
 * DateService is an object that includes a method for converting a Date object to a locale date string.
 *
 * @property {function} toLocaleDateString - A method that converts a Date object to a locale date string.
 */
const DateService = {
  /**
   * toLocaleDateString is a method that converts a Date object to a locale date string.
   * It uses the toLocaleDateString method of the Date object with 'en-US' as the locale and options for the year, month, and day.
   *
   * @param {Date} date - The Date object to convert.
   * @returns {string} A string representing the date in the format 'month day, year'.
   */
  toLocaleDateString(date: Date): string {
    let options: Intl.DateTimeFormatOptions = { year: 'numeric', month: 'short', day: 'numeric' };
    return date.toLocaleDateString('en-US', options);
  },
}

export default DateService;
