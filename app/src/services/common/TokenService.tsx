import AsyncStorage from '@react-native-async-storage/async-storage';

/**
 * TokenService is an object that includes methods for setting and getting the access token.
 *
 * @property {function} setToken - A method that sets the access token.
 * @property {function} getToken - A method that gets the access token.
 */
const TokenService = {

  /**
   * setToken is a method that sets the access token.
   * It stores the token in AsyncStorage under the key 'accessToken'.
   *
   * @param {string} token - The access token to set.
   * @returns {Promise<void>} A promise that resolves to undefined.
   */
  async setToken(token: string): Promise<void> {
    await AsyncStorage.setItem('accessToken', token);
  },

  /**
   * getToken is a method that gets the access token.
   * It retrieves the token from AsyncStorage under the key 'accessToken'.
   *
   * @returns {Promise<string | null>} A promise that resolves to the access token or null if it does not exist.
   */
  async getToken(): Promise<string | null> {
    return AsyncStorage.getItem('accessToken');
  },
}

export default TokenService;
