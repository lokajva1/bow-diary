import {ArrowPositionImpl} from './ArrowPosition.tsx';

/**
 * RoundEnd is an interface that defines the structure for a round end.
 *
 * @interface
 * @property {string} id - The unique identifier for the round end.
 * @property {ArrowPositionImpl[]} arrowPositions - An array of ArrowPositionImpl objects that represents the arrow positions for the round end.
 * @property {() => number} getScore - A function that returns the score for the round end.
 * @property {() => number} getArrowsShot - A function that returns the number of arrows shot for the round end.
 */
export interface RoundEnd {
  id: string;
  arrowPositions: ArrowPositionImpl[];

  getScore(): number;
  getArrowsShot(): number;
}

/**
 * RoundEndImpl is a class that implements the RoundEnd interface.
 *
 * @class
 * @property {string} id - The unique identifier for the round end.
 * @property {ArrowPositionImpl[]} arrowPositions - An array of ArrowPositionImpl objects that represents the arrow positions for the round end.
 *
 * @example
 * const roundEnd = new RoundEndImpl('1', [new ArrowPositionImpl('1', 10, 20, 'X')]);
 */
export class RoundEndImpl implements RoundEnd {
  id: string;
  arrowPositions: ArrowPositionImpl[];

  /**
   * Creates a new instance of the RoundEndImpl class.
   *
   * @param {string} id - The unique identifier for the round end.
   * @param {ArrowPositionImpl[]} arrowPositions - An array of ArrowPositionImpl objects that represents the arrow positions for the round end.
   */
  constructor(
    id: string = '',
    arrowPositions: ArrowPositionImpl[] = []
  ) {
    this.id = id;
    this.arrowPositions = arrowPositions;
  }

  /**
   * getArrowsShot is a method that returns the number of arrows shot for the round end.
   *
   * @returns {number} The number of arrows shot for the round end.
   */
  getArrowsShot(): number {
    return this.arrowPositions.findIndex((arrowPosition) => arrowPosition.scoreLabel === '') + 1 || this.arrowPositions.length
  }

  /**
   * getScore is a method that returns the score for the round end.
   *
   * @returns {number} The score for the round end.
   */
  getScore(): number {
    // sum score in each arrow position
    return this.arrowPositions.reduce((acc, arrowPosition) => acc + arrowPosition.getScore(), 0);
  }
}
