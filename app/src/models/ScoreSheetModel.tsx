import {ArrowPosition} from './ArrowPosition.tsx';
import {TargetFaceModel, TargetFaceModelImpl} from './TargetFaceModel.tsx';

/**
 * ScoreSheetModel is an interface that defines the structure for a score sheet model.
 *
 * @interface
 * @property {string} roundEndId - The unique identifier for the round end.
 * @property {number} roundEndPosition - The position of the round end.
 * @property {number} maxArrows - The maximum number of arrows.
 * @property {ArrowPosition[]} arrowPositions - An array of ArrowPosition objects that represents the arrow positions.
 * @property {number} scoreFromPreviousRounds - The score from previous rounds.
 * @property {TargetFaceModel} targetFace - The target face model.
 * @property {(scoreSheet: ScoreSheetModel) => ScoreSheetModel} copy - A function that takes a ScoreSheetModel and returns a new ScoreSheetModel.
 * @property {() => number} getScore - A function that returns the score.
 * @property {() => number} getTotalScore - A function that returns the total score.
 */
export interface ScoreSheetModel {
  roundEndId: string;
  roundEndPosition: number;
  maxArrows: number;
  arrowPositions: ArrowPosition[];
  scoreFromPreviousRounds: number;
  targetFace: TargetFaceModel;

  copy(scoreSheet: ScoreSheetModel): ScoreSheetModel;
  getScore(): number;
  getTotalScore(): number;
}

/**
 * ScoreSheetModelImpl is a class that implements the ScoreSheetModel interface.
 *
 * @class
 * @property {string} roundEndId - The unique identifier for the round end.
 * @property {number} roundEndPosition - The position of the round end.
 * @property {number} maxArrows - The maximum number of arrows.
 * @property {ArrowPosition[]} arrowPositions - An array of ArrowPosition objects that represents the arrow positions.
 * @property {number} scoreFromPreviousRounds - The score from previous rounds.
 * @property {TargetFaceModel} targetFace - The target face model.
 *
 * @example
 * const scoreSheetModel = new ScoreSheetModelImpl('1', 1, 3, [new ArrowPositionImpl('1', 10, 20, 'X')], 30, new TargetFaceModelImpl());
 */
export class ScoreSheetModelImpl implements ScoreSheetModel {
  roundEndId: string;
  roundEndPosition: number;
  maxArrows: number;
  arrowPositions: ArrowPosition[];
  scoreFromPreviousRounds: number;
  targetFace: TargetFaceModel;

  /**
   * Creates a new instance of the ScoreSheetModelImpl class.
   *
   * @param {string} roundEndId - The unique identifier for the round end.
   * @param {number} roundEndPosition - The position of the round end.
   * @param {number} maxArrows - The maximum number of arrows.
   * @param {ArrowPosition[]} arrowPositions - An array of ArrowPosition objects that represents the arrow positions.
   * @param {number} scoreFromPreviousRounds - The score from previous rounds.
   * @param {TargetFaceModel} targetFace - The target face model.
   */
  constructor(
    roundEndId: string = '',
    roundEndPosition: number = 1,
    maxArrows: number = 0,
    arrowPositions: ArrowPosition[] = [],
    scoreFromPreviousRounds: number = 0,
    targetFace: TargetFaceModel = new TargetFaceModelImpl()
  ) {
    this.roundEndId = roundEndId;
    this.roundEndPosition = roundEndPosition;
    this.maxArrows = maxArrows;
    this.arrowPositions = arrowPositions;
    this.scoreFromPreviousRounds = scoreFromPreviousRounds;
    this.targetFace = targetFace;
  }

  /**
   * copy is a method that takes a ScoreSheetModel and returns a new ScoreSheetModel.
   *
   * @param {ScoreSheetModel} scoreSheet - The score sheet model to copy.
   * @returns {ScoreSheetModel} A new ScoreSheetModel.
   */
  copy(scoreSheet: ScoreSheetModel): ScoreSheetModel {
    return new ScoreSheetModelImpl(
      scoreSheet.roundEndId,
      scoreSheet.roundEndPosition,
      scoreSheet.maxArrows,
      scoreSheet.arrowPositions,
      scoreSheet.scoreFromPreviousRounds,
      scoreSheet.targetFace
    );
  }

  /**
   * getScore is a method that returns the score.
   *
   * @returns {number} The score.
   */
  getScore(): number {
    return this.arrowPositions.reduce((acc, arrow) => acc + arrow.getScore(), 0);
  }

  /**
   * getTotalScore is a method that returns the total score.
   *
   * @returns {number} The total score.
   */
  getTotalScore(): number {
    return this.scoreFromPreviousRounds + this.getScore();
  }
}
