/**
 * ArrowPosition is an interface that defines the structure for an arrow position.
 *
 * @interface
 * @property {string} id - The unique identifier for the arrow position.
 * @property {number} x - The x-coordinate for the arrow position.
 * @property {number} y - The y-coordinate for the arrow position.
 * @property {string} scoreLabel - The score label for the arrow position.
 * @property {() => number} getScore - A function that returns the score for the arrow position.
 */
export interface ArrowPosition {
  id: string;
  x: number;
  y: number;
  scoreLabel: string;

  getScore(): number;
}

/**
 * ArrowPositionImpl is a class that implements the ArrowPosition interface.
 *
 * @class
 * @property {string} id - The unique identifier for the arrow position.
 * @property {number} x - The x-coordinate for the arrow position.
 * @property {number} y - The y-coordinate for the arrow position.
 * @property {string} scoreLabel - The score label for the arrow position.
 *
 * @example
 * const arrowPosition = new ArrowPositionImpl('1', 10, 20, 'X');
 */
export class ArrowPositionImpl implements ArrowPosition {
  id: string;
  x: number;
  y: number;
  scoreLabel: string;

  /**
   * Creates a new instance of the ArrowPositionImpl class.
   *
   * @param {string} id - The unique identifier for the arrow position.
   * @param {number} x - The x-coordinate for the arrow position.
   * @param {number} y - The y-coordinate for the arrow position.
   * @param {string} scoreLabel - The score label for the arrow position.
   */
  constructor(id: string, x: number = 0, y: number = 0, scoreLabel: string = "") {
    this.id = id;
    this.x = x;
    this.y = y;
    this.scoreLabel = scoreLabel;
  }

  /**
   * getScore is a method that returns the score for the arrow position.
   *
   * @returns {number} The score for the arrow position.
   */
  getScore(): number {
    if (this.scoreLabel === "X") {
      return 10;
    }
    if (this.scoreLabel === "M") {
      return 0;
    }
    return parseInt(this.scoreLabel);
  }
}
