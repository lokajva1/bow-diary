/**
 * BowModel is an interface that defines the structure for a bow model.
 *
 * @interface
 * @property {string} id - The unique identifier for the bow model.
 * @property {string} name - The name of the bow model.
 * @property {BowTypeModel} bowType - The type of the bow model.
 * @property {number} strength - The strength of the bow model.
 * @property {string} notes - The notes for the bow model.
 */
export interface BowModel {
  id: string;
  name: string;
  bowType: BowTypeModel;
  strength: number;
  notes: string;
}

/**
 * BowModelImpl is a class that implements the BowModel interface.
 *
 * @class
 * @property {string} id - The unique identifier for the bow model.
 * @property {string} name - The name of the bow model.
 * @property {BowTypeModel} bowType - The type of the bow model.
 * @property {number} strength - The strength of the bow model.
 * @property {string} notes - The notes for the bow model.
 *
 * @example
 * const bowModel = new BowModelImpl('1', 'Bow Name', new BowTypeModelImpl(), 10, 'Notes about the bow');
 */
export class BowModelImpl implements BowModel {
  id: string;
  name: string;
  bowType: BowTypeModel;
  strength: number;
  notes: string;

  /**
   * Creates a new instance of the BowModelImpl class.
   *
   * @param {string} id - The unique identifier for the bow model.
   * @param {string} name - The name of the bow model.
   * @param {BowTypeModel} bowType - The type of the bow model.
   * @param {number} strength - The strength of the bow model.
   * @param {string} notes - The notes for the bow model.
   */
  constructor(
    id: string = '',
    name: string = '',
    bowType: BowTypeModel = new BowTypeModelImpl(),
    strength: number = 0,
    notes: string = ''
  ) {
    this.id = id;
    this.name = name;
    this.bowType = bowType;
    this.strength = strength;
    this.notes = notes;
  }
}

/**
 * BowTypeModel is an interface that defines the structure for a bow type model.
 *
 * @interface
 * @property {string} id - The unique identifier for the bow type model.
 * @property {string} name - The name of the bow type model.
 */
interface BowTypeModel {
  id: string;
  name: string;
}

/**
 * BowTypeModelImpl is a class that implements the BowTypeModel interface.
 *
 * @class
 * @property {string} id - The unique identifier for the bow type model.
 * @property {string} name - The name of the bow type model.
 *
 * @example
 * const bowTypeModel = new BowTypeModelImpl('1', 'Bow Type Name');
 */
export class BowTypeModelImpl implements BowTypeModel {
  id: string;
  name: string;

  /**
   * Creates a new instance of the BowTypeModelImpl class.
   *
   * @param {string} id - The unique identifier for the bow type model.
   * @param {string} name - The name of the bow type model.
   */
  constructor(
    id: string = '',
    name: string = ''
  ) {
    this.id = id;
    this.name = name;
  }
}
