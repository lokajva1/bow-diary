import {TrainingRoundModel, TrainingRoundRequestModel} from './TrainingRoundModel.tsx';
import {BowModel, BowModelImpl} from './BowModel.tsx';

/**
 * TrainingModel is an interface that defines the structure for a training model.
 *
 * @interface
 * @property {string} id - The unique identifier for the training model.
 * @property {string} name - The name of the training model.
 * @property {TrainingRoundModel[]} rounds - An array of TrainingRoundModel objects that represents the rounds for the training model.
 * @property {BowModel} bow - The bow model for the training model.
 * @property {Date} date - The date of the training model.
 */
export interface TrainingModel {
  id: string;
  name: string;
  rounds: TrainingRoundModel[];
  bow: BowModel;
  date: Date;

  /**
   * getActiveRound is a method that returns the active round.
   *
   * @returns {number} The active round.
   */
  getActiveRound(): number;

  /**
   * getScore is a method that returns the score.
   *
   * @returns {number} The score.
   */
  getScore(): number;

  /**
   * getMaxScore is a method that returns the maximum score.
   *
   * @returns {number} The maximum score.
   */
  getMaxScore(): number;
}

/**
 * TrainingModelImpl is a class that implements the TrainingModel interface.
 *
 * @class
 * @property {string} id - The unique identifier for the training model.
 * @property {string} name - The name of the training model.
 * @property {TrainingRoundModel[]} rounds - An array of TrainingRoundModel objects that represents the rounds for the training model.
 * @property {BowModel} bow - The bow model for the training model.
 * @property {Date} date - The date of the training model.
 */
export class TrainingModelImpl implements TrainingModel {
  id: string;
  name: string;
  rounds: TrainingRoundModel[];
  bow: BowModel;
  date: Date;

  /**
   * Creates a new instance of the TrainingModelImpl class.
   *
   * @param {string} id - The unique identifier for the training model.
   * @param {string} name - The name of the training model.
   * @param {TrainingRoundModel[]} rounds - An array of TrainingRoundModel objects that represents the rounds for the training model.
   * @param {BowModel} bow - The bow model for the training model.
   * @param {Date} date - The date of the training model.
   */
  constructor(
    id: string = '',
    name: string = '',
    rounds: TrainingRoundModel[] = [],
    bow: BowModel = new BowModelImpl(), // Assuming BowModel has a default constructor
    date: Date = new Date()
  ) {
    this.id = id;
    this.name = name;
    this.rounds = rounds;
    this.bow = bow;
    this.date = date;
  }

  /**
   * getActiveRound is a method that returns the active round.
   *
   * @returns {number} The active round.
   */
  getActiveRound(): number {
    const activeRoundIdx =  this.rounds.findIndex(round => round.getActiveRoundEnd() !== undefined)
    return activeRoundIdx !== -1 ? activeRoundIdx + 1 : this.rounds.length;
  }

  /**
   * getScore is a method that returns the score.
   *
   * @returns {number} The score.
   */
  getScore(): number {
    return this.rounds.reduce((acc, round) => acc + round.getScore(), 0);
  }

  /**
   * getMaxScore is a method that returns the maximum score.
   *
   * @returns {number} The maximum score.
   */
  getMaxScore(): number {
    // sum max score in each round
    return this.rounds.reduce((acc, round) => acc + round.getMaxScore(), 0);
  }
}

/**
 * TrainingRequestModel is an interface that defines the structure for a training request model.
 *
 * @interface
 * @property {string} userId - The user identifier for the training request model.
 * @property {string} name - The name of the training request model.
 * @property {Date} date - The date of the training request model.
 * @property {string} bowId - The bow identifier for the training request model.
 * @property {TrainingRoundRequestModel[]} rounds - An array of TrainingRoundRequestModel objects that represents the rounds for the training request model.
 */
export interface TrainingRequestModel {
  userId: string;
  name: string;
  date: Date;
  bowId: string;
  rounds: TrainingRoundRequestModel[];
}

/**
 * TrainingRequestModelImpl is a class that implements the TrainingRequestModel interface.
 *
 * @class
 * @property {string} userId - The user identifier for the training request model.
 * @property {string} name - The name of the training request model.
 * @property {Date} date - The date of the training request model.
 * @property {string} bowId - The bow identifier for the training request model.
 * @property {TrainingRoundRequestModel[]} rounds - An array of TrainingRoundRequestModel objects that represents the rounds for the training request model.
 */
export class TrainingRequestModelImpl implements TrainingRequestModel {
  userId: string;
  name: string;
  date: Date;
  bowId: string;
  rounds: TrainingRoundRequestModel[];

  /**
   * Creates a new instance of the TrainingRequestModelImpl class.
   *
   * @param {string} userId - The user identifier for the training request model.
   * @param {string} name - The name of the training request model.
   * @param {Date} date - The date of the training request model.
   * @param {string} bowId - The bow identifier for the training request model.
   * @param {TrainingRoundRequestModel[]} rounds - An array of TrainingRoundRequestModel objects that represents the rounds for the training request model.
   */
  constructor(
    userId: string = '',
    name: string = '',
    date: Date = new Date(),
    bowId: string = '',
    rounds: TrainingRoundRequestModel[] = []
  ) {
    this.userId = userId;
    this.name = name;
    this.date = date;
    this.bowId = bowId;
    this.rounds = rounds;
  }
}
