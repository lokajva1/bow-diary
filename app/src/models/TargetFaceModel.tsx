/**
 * TargetFaceModel is an interface that defines the structure for a target face model.
 *
 * @interface
 * @property {string} id - The unique identifier for the target face model.
 * @property {string} name - The name of the target face model.
 * @property {number} diameter - The diameter of the target face model.
 * @property {string} type - The type of the target face model.
 * @property {number[]} ringsDistance - An array of numbers that represents the rings distance for the target face model.
 */
export interface TargetFaceModel {
  id: string;
  name: string;
  diameter: number;
  type: string;
  ringsDistance: number[];
}

/**
 * TargetFaceModelImpl is a class that implements the TargetFaceModel interface.
 *
 * @class
 * @property {string} id - The unique identifier for the target face model.
 * @property {string} name - The name of the target face model.
 * @property {number} diameter - The diameter of the target face model.
 * @property {string} type - The type of the target face model.
 * @property {number[]} ringsDistance - An array of numbers that represents the rings distance for the target face model.
 *
 * @example
 * const targetFaceModel = new TargetFaceModelImpl('1', 'Target Face Name', 10, 'Type', [1, 2, 3]);
 */
export class TargetFaceModelImpl implements TargetFaceModel{
  id: string;
  name: string;
  diameter: number;
  type: string;
  ringsDistance: number[];

  /**
   * Creates a new instance of the TargetFaceModelImpl class.
   *
   * @param {string} id - The unique identifier for the target face model.
   * @param {string} name - The name of the target face model.
   * @param {number} diameter - The diameter of the target face model.
   * @param {string} type - The type of the target face model.
   * @param {number[]} ringsDistance - An array of numbers that represents the rings distance for the target face model.
   */
  constructor(
    id: string = '',
    name: string = '',
    diameter: number = 0,
    type: string = '',
    ringsDistance: number[] = []) {
    this.id = id;
    this.name = name;
    this.diameter = diameter;
    this.type = type;
    this.ringsDistance = ringsDistance;
  }
}
