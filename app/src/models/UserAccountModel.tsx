// User acount model
/**
 * UserAccountModel is an interface that defines the structure for a user account model.
 *
 * @interface
 * @property {string} username - The username of the user account.
 * @property {string} password - The password of the user account.
 */
export interface UserAccountModel {
  username: string;
  password: string;
}

/**
 * UserAccountModelImpl is a class that implements the UserAccountModel interface.
 *
 * @class
 * @property {string} username - The username of the user account.
 * @property {string} password - The password of the user account.
 *
 * @example
 * const userAccount = new UserAccountModelImpl('username', 'password');
 */
export class UserAccountModelImpl implements UserAccountModel {
  /**
   * Creates a new instance of the UserAccountModelImpl class.
   *
   * @param {string} username - The username of the user account.
   * @param {string} password - The password of the user account.
   */
  constructor(
    public username: string = '',
    public password: string = '',
  ) {
    this.username = username;
    this.password = password;
  }
}
