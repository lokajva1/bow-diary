import {TargetFaceModel, TargetFaceModelImpl} from './TargetFaceModel.tsx';
import {RoundEnd} from './RoundEnd.tsx';

/**
 * TrainingRoundModel is an interface that defines the structure for a training round model.
 *
 * @interface
 * @property {string} id - The unique identifier for the training round model.
 * @property {number} roundNumber - The round number of the training round model.
 * @property {RoundEnd[]} roundEnds - An array of RoundEnd objects that represents the round ends for the training round model.
 * @property {TargetFaceModel} targetFace - The target face model for the training round model.
 * @property {number} numberOfEnds - The number of ends for the training round model.
 * @property {number} arrowInEachEnd - The number of arrows in each end for the training round model.
 * @property {number} distance - The distance for the training round model.
 * @property {string} note - The note for the training round model.
 */
export interface TrainingRoundModel {
  id: string;
  roundNumber: number;
  roundEnds: RoundEnd[];
  targetFace: TargetFaceModel;
  numberOfEnds: number;
  arrowInEachEnd: number;
  distance: number;
  note: string;

  /**
   * getScore is a method that returns the score.
   *
   * @returns {number} The score.
   */
  getScore(): number;

  /**
   * getMaxScore is a method that returns the maximum score.
   *
   * @returns {number} The maximum score.
   */
  getMaxScore(): number;

  /**
   * getMaxScorePerRoundEnd is a method that returns the maximum score per round end.
   *
   * @returns {number} The maximum score per round end.
   */
  getMaxScorePerRoundEnd(): number;

  /**
   * getActiveRoundEnd is a method that returns the active round end.
   *
   * @returns {RoundEnd | undefined} The active round end.
   */
  getActiveRoundEnd(): RoundEnd | undefined;

  /**
   * getActiveRoundEndIdx is a method that returns the active round end index.
   *
   * @returns {number} The active round end index.
   */
  getActiveRoundEndIdx(): number;
}

/**
 * TrainingRoundModelImpl is a class that implements the TrainingRoundModel interface.
 *
 * @class
 * @property {string} id - The unique identifier for the training round model.
 * @property {number} roundNumber - The round number of the training round model.
 * @property {RoundEnd[]} roundEnds - An array of RoundEnd objects that represents the round ends for the training round model.
 * @property {TargetFaceModel} targetFace - The target face model for the training round model.
 * @property {number} numberOfEnds - The number of ends for the training round model.
 * @property {number} arrowInEachEnd - The number of arrows in each end for the training round model.
 * @property {number} distance - The distance for the training round model.
 * @property {string} note - The note for the training round model.
 *
 * @example
 * const trainingRoundModel = new TrainingRoundModelImpl('1', 1, [new RoundEndImpl()], new TargetFaceModelImpl(), 3, 6, 50, 'Note about the training round');
 */
export class TrainingRoundModelImpl implements TrainingRoundModel {
  id: string;
  roundNumber: number;
  roundEnds: RoundEnd[];
  targetFace: TargetFaceModel;
  numberOfEnds: number;
  arrowInEachEnd: number;
  distance: number;
  note: string;

  /**
   * Creates a new instance of the TrainingRoundModelImpl class.
   *
   * @param {string} id - The unique identifier for the training round model.
   * @param {number} roundNumber - The round number of the training round model.
   * @param {RoundEnd[]} roundEnds - An array of RoundEnd objects that represents the round ends for the training round model.
   * @param {TargetFaceModel} targetFace - The target face model for the training round model.
   * @param {number} numberOfEnds - The number of ends for the training round model.
   * @param {number} arrowInEachEnd - The number of arrows in each end for the training round model.
   * @param {number} distance - The distance for the training round model.
   * @param {string} note - The note for the training round model.
   */
  constructor(
    id: string = '',
    roundNumber: number = 0,
    roundEnds: RoundEnd[] = [],
    targetFace: TargetFaceModel = new TargetFaceModelImpl(),
    numberOfEnds: number = 0,
    arrowInEachEnd: number = 0,
    distance: number = 0,
    note: string = ''
  ) {
    this.id = id;
    this.roundNumber = roundNumber;
    this.roundEnds = roundEnds;
    this.targetFace = targetFace;
    this.numberOfEnds = numberOfEnds;
    this.arrowInEachEnd = arrowInEachEnd;
    this.distance = distance;
    this.note = note;
  }

  /**
   * getScore is a method that returns the score.
   *
   * @returns {number} The score.
   */
  getScore(): number {
    return this.roundEnds.reduce((acc, arrowPosition) => acc + arrowPosition.getScore(), 0);
  }

  /**
   * getMaxScore is a method that returns the maximum score.
   *
   * @returns {number} The maximum score.
   */
  getMaxScore(): number {
    return this.numberOfEnds * this.arrowInEachEnd * 10;
  }

  /**
   * getMaxScorePerRoundEnd is a method that returns the maximum score per round end.
   *
   * @returns {number} The maximum score per round end.
   */
  getMaxScorePerRoundEnd(): number {
    return this.arrowInEachEnd * 10;
  }

  /**
   * getActiveRoundEnd is a method that returns the active round end.
   *
   * @returns {RoundEnd | undefined} The active round end.
   */
  getActiveRoundEnd(): RoundEnd | undefined {
    return this.roundEnds.find((end) => end.arrowPositions.length < this.arrowInEachEnd);
  }

  /**
   * getActiveRoundEndIdx is a method that returns the active round end index.
   *
   * @returns {number} The active round end index.
   */
  getActiveRoundEndIdx(): number {
    const idx = this.roundEnds.findIndex((end) => end.arrowPositions.length < this.arrowInEachEnd);
    return idx === -1 ? this.roundEnds.length : idx + 1;
  }
}

/**
 * TrainingRoundRequestModel is an interface that defines the structure for a training round request model.
 *
 * @interface
 * @property {string} targetFaceId - The target face identifier for the training round request model.
 * @property {number} numberOfEnds - The number of ends for the training round request model.
 * @property {number} arrowInEachEnd - The number of arrows in each end for the training round request model.
 * @property {number} distance - The distance for the training round request model.
 * @property {string} note - The note for the training round request model.
 */
export interface TrainingRoundRequestModel {
  targetFaceId: string;
  numberOfEnds: number;
  arrowInEachEnd: number;
  distance: number;
  note: string;
}

/**
 * TrainingRoundRequestModelImpl is a class that implements the TrainingRoundRequestModel interface.
 *
 * @class
 * @property {string} targetFaceId - The target face identifier for the training round request model.
 * @property {number} numberOfEnds - The number of ends for the training round request model.
 * @property {number} arrowInEachEnd - The number of arrows in each end for the training round request model.
 * @property {number} distance - The distance for the training round request model.
 * @property {string} note - The note for the training round request model.
 *
 * @example
 * const trainingRoundRequestModel = new TrainingRoundRequestModelImpl('1', 3, 6, 50, 'Note about the training round request');
 */
export class TrainingRoundRequestModelImpl implements TrainingRoundRequestModel {
  targetFaceId: string;
  numberOfEnds: number;
  arrowInEachEnd: number;
  distance: number;
  note: string;

  /**
   * Creates a new instance of the TrainingRoundRequestModelImpl class.
   *
   * @param {string} targetFaceId - The target face identifier for the training round request model.
   * @param {number} numberOfEnds - The number of ends for the training round request model.
   * @param {number} arrowInEachEnd - The number of arrows in each end for the training round request model.
   * @param {number} distance - The distance for the training round request model.
   * @param {string} note - The note for the training round request model.
   */
  constructor(
    targetFaceId: string = '',
    numberOfEnds: number = 0,
    arrowInEachEnd: number = 0,
    distance: number = 0,
    note: string = ''
  ) {
    this.targetFaceId = targetFaceId;
    this.numberOfEnds = numberOfEnds;
    this.arrowInEachEnd = arrowInEachEnd;
    this.distance = distance;
    this.note = note;
  }
}
