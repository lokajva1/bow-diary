/**
 * SharedStyles is an object that exports a set of color values.
 * Each property of the object represents a color, and the value of each property is a string that represents the hexadecimal color code for that color.
 * This object can be used in other parts of the code to reference these colors by their property names, which can make the code more readable and maintainable.
 */
export default {
  white: '#FFFFFF',
  grey: '#EEECEC',
  darkGrey: '#b7b7b7',
  black : '#000000',
  green: '#03D024',
  yellow: '#EDC700',
};
