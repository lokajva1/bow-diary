/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';

// Importing necessary components and screens
import LoginScreen from './src/screens/login/LoginScreen.tsx';
import {DefaultTheme, NavigationContainer, Theme} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import TrainingListScreen from './src/screens/training-list/TrainingListScreen.tsx';
import SharedStyles from './src/assets/style/SharedStyles.tsx';
import TrainingCreationScreen from './src/screens/training-creation/TrainingCreationScreen.tsx';
import BowListScreen from './src/screens/bow-list/BowListScreen.tsx';
import TrainingRoundCreationScreen from './src/screens/training-round-creation/TrainingRoundCreationScreen.tsx';
import {LogBox} from 'react-native';
import TrainingRoundListScreen from './src/screens/training-round-list/TrainingRoundListScreen.tsx';
import RoundEndListScreen from './src/screens/round-end-list/RoundEndListScreen.tsx';
import ArrowPositioningScreen from './src/screens/arrow-positioning-screen/ArrowPositioningScreen.tsx';
import {navigationRef} from './src/services/common/NavigationReferenceService.tsx';
import TrainingStatisticScreen from './src/screens/training-statistic-screen/TrainingStatisticScreen.tsx';

// Creating a stack navigator
const Stack = createNativeStackNavigator();

// Defining a custom theme for the navigation container
const navigationContainerTheme: Theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: SharedStyles.grey
  }
};

// Ignoring specific logs
LogBox.ignoreLogs([
  'Non-serializable values were found in the navigation state.',
]);

/**
 * Main App component
 * @returns {React.JSX.Element} The rendered App component
 */
function App(): React.JSX.Element {
  return (
    <NavigationContainer theme={navigationContainerTheme} ref={navigationRef}>
      <Stack.Navigator
        screenOptions={{
          headerTintColor: SharedStyles.white,
          headerStyle: {
            backgroundColor: SharedStyles.green
          },
          headerTitleAlign: 'center'
        }}
      >
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="My Trainings" component={TrainingListScreen} />
        <Stack.Screen name="Training Creation" component={TrainingCreationScreen} />
        <Stack.Screen name="Training Rounds" component={TrainingRoundListScreen} />
        <Stack.Screen name="Training Round Ends" component={RoundEndListScreen} />
        <Stack.Screen name="Arrow Positioning" component={ArrowPositioningScreen} />
        <Stack.Screen name="Bows" component={BowListScreen} />
        <Stack.Screen name="Training Round Creation" component={TrainingRoundCreationScreen} />
        <Stack.Screen name="Training Statistic" component={TrainingStatisticScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

// Exporting the App component
export default App;
