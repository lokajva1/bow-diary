package com.cvut.api.training.web.controller;

import com.cvut.api.config.GlobalExceptionHandler;
import com.cvut.api.training.domain.model.TrainingEntity;
import com.cvut.api.training.service.TrainingService;
import com.cvut.api.training.web.model.TrainingRequest;
import com.cvut.api.training_round.web.model.TrainingRoundRequest;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class TrainingControllerTest {

    @InjectMocks
    private TrainingController trainingController;

    @Mock
    private TrainingService trainingService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void getTraining_withExistingId_returnsTrainingEntity() {
        UUID id = UUID.randomUUID();
        TrainingEntity trainingEntity = new TrainingEntity();
        trainingEntity.setId(id);

        when(trainingService.getTraining(id)).thenReturn(trainingEntity);

        ResponseEntity<TrainingEntity> response = trainingController.getTraining(id);

        assertEquals(trainingEntity, response.getBody());
    }

    @Test
    public void getTraining_withInvalidId_returnsNotFound() {
        UUID id = UUID.randomUUID();

        when(trainingService.getTraining(id)).thenThrow(new EntityNotFoundException());

        assertThrows(EntityNotFoundException.class, () -> trainingController.getTraining(id));
    }

    @Test
    public void deleteTraining_withExistingId_returnsNoContent() {
        UUID id = UUID.randomUUID();

        ResponseEntity<Void> response = trainingController.deleteTraining(id);

        verify(trainingService, times(1)).deleteTraining(id);
        assertEquals(204, response.getStatusCodeValue());
    }

    @Test
    public void deleteTraining_withInvalidId_returnsNotFound() {
        UUID id = UUID.randomUUID();

        doThrow(new EntityNotFoundException()).when(trainingService).deleteTraining(id);

        assertThrows(EntityNotFoundException.class, () -> trainingController.deleteTraining(id));
    }

    @Test
    public void createTraining_withValidRequest_returnsTrainingEntity() {
        TrainingRequest trainingRequest = new TrainingRequest(
                UUID.randomUUID().toString(),
                "test training",
                new Date(),
                UUID.randomUUID().toString(),
                List.of(
                        new TrainingRoundRequest(
                                UUID.randomUUID().toString(),
                                4,
                                4,
                                70,
                                "test round"
                        )
                )
        );
        TrainingEntity trainingEntity = new TrainingEntity();

        when(trainingService.createTraining(trainingRequest)).thenReturn(trainingEntity);

        ResponseEntity<TrainingEntity> response = trainingController.createTraining(trainingRequest);

        assertEquals(trainingEntity, response.getBody());
    }
}