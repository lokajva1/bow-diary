package com.cvut.api.training.service;

import com.cvut.api.bow.domain.model.BowEntity;
import com.cvut.api.bow.service.BowService;
import com.cvut.api.target_face.domain.model.TargetFaceEntity;
import com.cvut.api.target_face.domain.repository.TargetFaceRepository;
import com.cvut.api.training.domain.model.TrainingEntity;
import com.cvut.api.training.domain.repository.TrainingRepository;
import com.cvut.api.training.web.model.TrainingRequest;
import com.cvut.api.training_round.web.model.TrainingRoundRequest;
import com.cvut.api.user_account.domain.model.UserAccountEntity;
import com.cvut.api.user_account.service.UserAccountService;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class TrainingServiceTest {

    @InjectMocks
    private TrainingService trainingService;

    @Mock
    private UserAccountService userAccountService;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private TargetFaceRepository targetFaceRepository;

    @Mock
    private BowService bowService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void createTraining_withEmptyRounds_returnsTrainingEntity() {
        TrainingRequest trainingRequest = TrainingRequest.builder()
                .bowId(UUID.randomUUID().toString())
                .userId(UUID.randomUUID().toString())
                .rounds(List.of())
                .build();

        UserAccountEntity userAccountEntity = new UserAccountEntity();
        BowEntity bowEntity = new BowEntity();

        when(userAccountService.getUser(any(UUID.class))).thenReturn(userAccountEntity);
        when(bowService.getBow(any(UUID.class))).thenReturn(bowEntity);
        when(trainingRepository.save(any(TrainingEntity.class))).thenAnswer(i -> i.getArguments()[0]);

        TrainingEntity result = trainingService.createTraining(trainingRequest);

        assertEquals(userAccountEntity, result.getUser());
        assertEquals(bowEntity, result.getBow());
    }

    @Test
    public void createTraining_withNewRound_returnsTrainingEntity() {
        TrainingRequest trainingRequest = TrainingRequest.builder()
                .bowId(UUID.randomUUID().toString())
                .userId(UUID.randomUUID().toString())
                .rounds(List.of(
                        TrainingRoundRequest.builder()
                                .targetFaceId(UUID.randomUUID().toString())
                                .numberOfEnds(2)
                                .arrowInEachEnd(2)
                                .distance(70)
                                .build()
                ))
                .build();

        UserAccountEntity userAccountEntity = new UserAccountEntity();
        BowEntity bowEntity = new BowEntity();
        TargetFaceEntity targetFaceEntity = new TargetFaceEntity();

        when(userAccountService.getUser(any(UUID.class))).thenReturn(userAccountEntity);
        when(bowService.getBow(any(UUID.class))).thenReturn(bowEntity);
        when(trainingRepository.save(any(TrainingEntity.class))).thenAnswer(i -> i.getArguments()[0]);
        when(targetFaceRepository.findById(any(UUID.class))).thenReturn(java.util.Optional.of(targetFaceEntity));

        TrainingEntity result = trainingService.createTraining(trainingRequest);

        assertEquals(userAccountEntity, result.getUser());
        assertEquals(bowEntity, result.getBow());
        assertEquals(1, result.getRounds().size());
        assertEquals(2, result.getRounds().get(0).getRoundEnds().size());
    }

    @Test
    public void createTraining_withNullUserId_throwsNullPointerException() {
        TrainingRequest trainingRequest = TrainingRequest.builder()
                .bowId(UUID.randomUUID().toString())
                .userId(null)
                .rounds(List.of())
                .build();

        assertThrows(NullPointerException.class, () -> trainingService.createTraining(trainingRequest));
    }

    @Test
    public void createTraining_withInvalidUserId_throwsNullPointerException() {
        TrainingRequest trainingRequest = TrainingRequest.builder()
                .bowId(UUID.randomUUID().toString())
                .userId("invalid")
                .rounds(List.of())
                .build();

        assertThrows(IllegalArgumentException.class, () -> trainingService.createTraining(trainingRequest));
    }

    @Test
    public void getTraining_withExistingId_returnsTrainingEntity() {
        UUID id = UUID.randomUUID();
        TrainingEntity trainingEntity = new TrainingEntity();

        when(trainingRepository.findById(id)).thenReturn(java.util.Optional.of(trainingEntity));

        TrainingEntity result = trainingService.getTraining(id);

        assertEquals(trainingEntity, result);
    }

    @Test
    public void getTraining_withNullId_throwsIllegalArgumentException() {
        assertThrows(NullPointerException.class, () -> trainingService.getTraining(null));
    }

    @Test
    public void getTraining_withNonExistingId_throwsEntityNotFoundException() {
        UUID id = UUID.randomUUID();

        when(trainingRepository.findById(id)).thenReturn(java.util.Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> trainingService.getTraining(id));
    }

    @Test
    public void deleteTraining_withExistingId_deletesTrainingEntity() {
        UUID id = UUID.randomUUID();
        TrainingEntity trainingEntity = new TrainingEntity();
        trainingEntity.setId(id);

        when(trainingRepository.findById(id)).thenReturn(java.util.Optional.of(trainingEntity));

        trainingService.deleteTraining(id);

        verify(trainingRepository, times(1)).deleteById(id);
    }

    @Test
    public void deleteTraining_withNonExistingId_throwsEntityNotFoundException() {
        UUID id = UUID.randomUUID();

        when(trainingRepository.findById(id)).thenReturn(java.util.Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> trainingService.deleteTraining(id));
    }

    @Test
    public void deleteTraining_withNullId_throwsIllegalArgumentException() {
        assertThrows(NullPointerException.class, () -> trainingService.deleteTraining(null));
    }
}