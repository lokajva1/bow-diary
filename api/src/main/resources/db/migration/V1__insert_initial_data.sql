-- initial data
INSERT INTO user_account (id, password, username)
VALUES ('8c16a619-bbe3-4208-9b5b-e10a837f7202', '$2a$10$zTH1BX0IvxUjPu3JZhIwOusHBIoK2UnZXKPa.9G4rFml/6MsoKphC', 'username');

INSERT INTO bow_type (id, name)
VALUES ('6a2b2cd3-973c-41e5-afe5-17dd334c9e24', 'Recurve Bow');
INSERT INTO bow_type (id, name)
VALUES ('3a9844c6-20fc-46cd-9b67-94c3d3f058d9', 'Compound Bow');
INSERT INTO bow_type (id, name)
VALUES ('197cddf0-6b45-4a9b-8948-3bd9326a05bb', 'Bare Bow');

INSERT INTO bow (id, name, bow_type_id, strength, user_id)
VALUES ('123e4567-e89b-12d3-a456-426614174000', 'Me Recurve Bow', '6a2b2cd3-973c-41e5-afe5-17dd334c9e24', 30, '8c16a619-bbe3-4208-9b5b-e10a837f7202');

INSERT INTO target_face (id, name, diameter, type)
VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 'WA Full', 60, 'Recurve style (X-8)');

INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 15);
INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 25);
INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 45);
INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 64);
INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 85);
INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 105);
INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 125);
INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 145);
INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 160);
INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 180);
INSERT INTO target_face_entity_rings_distance (target_face_entity_id, rings_distance) VALUES ('4a94ccd0-ab80-43c3-97f5-62bf0fb5b08f', 205);
