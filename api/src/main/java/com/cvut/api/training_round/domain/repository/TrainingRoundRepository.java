package com.cvut.api.training_round.domain.repository;

import com.cvut.api.training_round.domain.model.TrainingRoundEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * TrainingRoundRepository is an interface that extends JpaRepository.
 * It is used to perform database operations on the TrainingRoundEntity.
 *
 * JpaRepository is a JPA specific extension of Repository. It contains the full API of CrudRepository and PagingAndSortingRepository.
 * So it contains API for basic CRUD operations and also API for pagination and sorting.
 *
 * TrainingRoundEntity is the entity that this repository works with.
 * UUID is the id type of the entity this repository works with.
 */
public interface TrainingRoundRepository extends JpaRepository<TrainingRoundEntity, UUID> {
}