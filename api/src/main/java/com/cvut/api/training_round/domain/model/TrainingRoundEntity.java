package com.cvut.api.training_round.domain.model;

import com.cvut.api.common.jpa.AbstractEntity;
import com.cvut.api.target_face.domain.model.TargetFaceEntity;
import com.cvut.api.round_end.domain.model.RoundEndEntity;
import com.cvut.api.training.domain.model.TrainingEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * Represents a training round in the system.
 * This is an entity class in the JPA sense, and is mapped to the "training_round" table in the database.
 *
 * @Getter is a Lombok annotation that generates getter methods for all fields.
 * @Setter is a Lombok annotation that generates setter methods for all fields.
 * @Entity is a JPA annotation that marks this class as an entity. This means that it is a JPA managed class and is mapped to a database table.
 * @NoArgsConstructor is a Lombok annotation that generates a no-argument constructor.
 * @AllArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field in the class.
 * @Builder is a Lombok annotation that generates a builder for this class.
 * @Table is a JPA annotation that specifies the name of the database table that this entity is mapped to.
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "training_round")
public class TrainingRoundEntity extends AbstractEntity {

    /**
     * Represents the round number of the training round.
     */
    private int roundNumber;

    /**
     * Represents the training session associated with this training round.
     * It is mapped to the "training_id" field in the TrainingEntity class.
     * @ManyToOne is a JPA annotation that indicates that this is the many side of a many-to-one relationship.
     * @JoinColumn indicates the column used for joining an entity association or element collection.
     * @JsonIgnore is a Jackson annotation that indicates that this field should not be included when the object is serialized to JSON.
     */
    @ManyToOne
    @JoinColumn(name = "training_id", nullable = false)
    @JsonIgnore
    private TrainingEntity training;

    /**
     * Represents the list of round ends associated with this training round.
     * It is mapped to the "trainingRound" field in the RoundEndEntity class.
     * @OneToMany is a JPA annotation that indicates that this is the one side of a one-to-many relationship.
     * The cascade attribute indicates the operations that must be cascaded to the target of the association.
     */
    @OneToMany(mappedBy = "trainingRound", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<RoundEndEntity> roundEnds;

    /**
     * Represents the target face associated with this training round.
     * It is mapped to the "target_face_id" field in the TargetFaceEntity class.
     * @ManyToOne is a JPA annotation that indicates that this is the many side of a many-to-one relationship.
     * @JoinColumn indicates the column used for joining an entity association or element collection.
     */
    @ManyToOne
    @JoinColumn(name = "target_face_id", nullable = false)
    private TargetFaceEntity targetFace;

    /**
     * Represents the number of ends in the training round.
     */
    private int numberOfEnds;

    /**
     * Represents the number of arrows in each end of the training round.
     */
    private int arrowInEachEnd;

    /**
     * Represents the distance of the training round.
     */
    private int distance;

    /**
     * Represents any additional notes for the training round.
     */
    private String note;
}