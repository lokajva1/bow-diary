package com.cvut.api.training_round.web.model;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;

/**
 * Represents a request to create or update a TrainingRound.
 * This is a record class, which is a special kind of class in Java that is used to model immutable data.
 * Each field in the record class has an associated accessor method, but no mutator method.
 *
 * @Builder is a Lombok annotation that generates a builder for this class.
 *
 * @NotBlank is a Jakarta Validation annotation that indicates that the annotated element must not be null or empty.
 * @Min is a Jakarta Validation annotation that indicates that the annotated element must be a number whose value must be higher or equal to the specified minimum.
 */
@Builder
public record TrainingRoundRequest(
        /**
         * Represents the target face id associated with the training round.
         * It must not be null or empty.
         */
        @NotBlank
        String targetFaceId,

        /**
         * Represents the number of ends in the training round.
         * It must be a positive integer.
         */
        @Min(1)
        Integer numberOfEnds,

        /**
         * Represents the number of arrows in each end of the training round.
         * It must be a positive integer.
         */
        @Min(1)
        Integer arrowInEachEnd,

        /**
         * Represents the distance of the training round.
         * It must be a positive integer.
         */
        @Min(1)
        Integer distance,

        /**
         * Represents any additional notes for the training round.
         * It can be null or empty.
         */
        String note) {
}