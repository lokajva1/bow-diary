package com.cvut.api.training_round.web.controller;


import com.cvut.api.training_round.domain.model.TrainingRoundEntity;
import com.cvut.api.training_round.service.TrainingRoundService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID; 

/**
 * TrainingRoundController is a REST controller that handles HTTP requests related to TrainingRoundEntity.
 * It uses TrainingRoundService to perform these operations.
 *
 * @RestController is a Spring annotation that marks this class as a REST controller. This means that Spring manages this class and can inject it where needed.
 * @RequestMapping is a Spring annotation that maps HTTP requests to handler methods of MVC and REST controllers.
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field that requires special handling.
 */
@RestController
@RequestMapping("/api/training-round")
@RequiredArgsConstructor
public class TrainingRoundController {

    /**
     * The TrainingRoundService that this controller uses to perform operations related to TrainingRoundEntity.
     */
    private final TrainingRoundService trainingRoundService;

    /**
     * Handles the GET /api/training-round/{id} endpoint.
     * Retrieves a TrainingRoundEntity by its id.
     *
     * @param id the id of the TrainingRoundEntity to retrieve.
     * @return a ResponseEntity containing the TrainingRoundEntity with the given id.
     */
    @GetMapping("/{id}")
    public ResponseEntity<TrainingRoundEntity> getTrainingRound(@PathVariable UUID id) {
        TrainingRoundEntity trainingRoundEntity = trainingRoundService.getTrainingRound(id);
        return ResponseEntity.ok(trainingRoundEntity);
    }
}