package com.cvut.api.training_round.service;

import com.cvut.api.training_round.domain.model.TrainingRoundEntity;
import com.cvut.api.training_round.domain.repository.TrainingRoundRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;

/**
 * TrainingRoundService is a service class that provides business logic for operations related to TrainingRoundEntity.
 * It uses TrainingRoundRepository to perform these operations.
 *
 * @Service is a Spring annotation that marks this class as a service. This means that Spring manages this class and can inject it where needed.
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field that requires special handling.
 */
@Service
@RequiredArgsConstructor
public class TrainingRoundService {

    /**
     * The TrainingRoundRepository that this service uses to perform database operations on TrainingRoundEntity.
     */
    private final TrainingRoundRepository trainingRoundRepository;

    /**
     * Retrieves a TrainingRoundEntity by its id.
     *
     * @param id the id of the TrainingRoundEntity to retrieve.
     * @return the TrainingRoundEntity with the given id.
     * @throws EntityNotFoundException if a TrainingRoundEntity with the given id is not found.
     */
    public TrainingRoundEntity getTrainingRound(@NonNull UUID id) {
        Objects.requireNonNull(id, "Id must not be null");
        return trainingRoundRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Training round with id " + id + " not found"));
    }
}