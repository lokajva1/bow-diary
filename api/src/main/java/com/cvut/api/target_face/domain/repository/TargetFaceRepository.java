package com.cvut.api.target_face.domain.repository;

import com.cvut.api.target_face.domain.model.TargetFaceEntity;
import java.util.UUID; 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * TargetFaceRepository is an interface that extends JpaRepository.
 * It is used to perform database operations on the TargetFaceEntity.
 *
 * @Repository is a Spring annotation that indicates that the decorated class is a repository.
 * A repository is a mechanism for encapsulating storage, retrieval, and search behavior which emulates a collection of objects.
 *
 * JpaRepository is a JPA specific extension of Repository. It contains the full API of CrudRepository and PagingAndSortingRepository.
 * So it contains API for basic CRUD operations and also API for pagination and sorting.
 *
 * TargetFaceEntity is the entity that this repository works with.
 * UUID is the id type of the entity this repository works with.
 */
@Repository
public interface TargetFaceRepository extends JpaRepository<TargetFaceEntity, UUID> {
}