package com.cvut.api.target_face.web.controller;

import com.cvut.api.target_face.domain.model.TargetFaceEntity;
import com.cvut.api.target_face.service.TargetFaceService;
import com.cvut.api.target_face.web.model.TargetFaceRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID; 

/**
 * TargetFaceController is a REST controller that provides HTTP endpoints for operations related to TargetFaceEntity.
 * It uses TargetFaceService to perform the business logic.
 *
 * @RestController is a Spring annotation that marks this class as a REST controller. This means that Spring manages this class and can inject it where needed.
 * @RequestMapping is a Spring annotation that is used to map web requests onto specific handler classes and/or handler methods.
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field that requires special handling. In this case, it generates a constructor that takes a TargetFaceService.
 */
@RestController
@RequestMapping("/api/target-face")
@RequiredArgsConstructor
public class TargetFaceController {

    /**
     * The TargetFaceService that this controller uses to perform the business logic.
     */
    private final TargetFaceService targetFaceService;

    /**
     * Retrieves all TargetFaceEntities.
     *
     * @return a list of all TargetFaceEntities.
     * @GetMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.GET).
     */
    @GetMapping
    public List<TargetFaceEntity> getAll() {
        return targetFaceService.getAll();
    }

    /**
     * Retrieves a TargetFaceEntity by its id.
     *
     * @param id the id of the TargetFaceEntity to retrieve.
     * @return the TargetFaceEntity with the given id.
     * @GetMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.GET).
     * @PathVariable is a Spring annotation which indicates that a method parameter should be bound to a URI template variable.
     */
    @GetMapping("/{id}")
    public TargetFaceEntity getById(@PathVariable UUID id) {
        return targetFaceService.getById(id);
    }

    /**
     * Creates a new TargetFaceEntity.
     *
     * @param targetFaceRequest the request object that contains the details of the new TargetFaceEntity.
     * @return the created TargetFaceEntity.
     * @PostMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.POST).
     * @Validated is a Spring annotation which indicates that a method parameter should be validated.
     * @RequestBody is a Spring annotation indicating a method parameter should be bound to the body of the web request.
     */
    @PostMapping
    public TargetFaceEntity create(@Validated @RequestBody TargetFaceRequest targetFaceRequest) {
        return targetFaceService.create(targetFaceRequest);
    }
}