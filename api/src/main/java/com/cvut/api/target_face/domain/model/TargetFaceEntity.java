package com.cvut.api.target_face.domain.model;

import com.cvut.api.common.jpa.AbstractEntity;
import com.cvut.api.training_round.domain.model.TrainingRoundEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * Represents a target face in a training round.
 * This is an entity class in the JPA sense, and is mapped to the "target_face" table in the database.
 *
 * @Getter is a Lombok annotation that generates getter methods for all fields.
 * @Setter is a Lombok annotation that generates setter methods for all fields.
 * @Entity is a JPA annotation that marks this class as an entity. This means that it is a JPA managed class and is mapped to a database table.
 * @NoArgsConstructor is a Lombok annotation that generates a no-argument constructor.
 * @AllArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field in the class.
 * @Builder is a Lombok annotation that generates a builder for this class.
 * @Table is a JPA annotation that specifies the name of the database table that this entity is mapped to.
 * @JsonIgnoreProperties is a Jackson annotation that indicates that the specified properties should not be included when the object is serialized to JSON.
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "target_face")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class TargetFaceEntity extends AbstractEntity {

    /**
     * Represents the list of training rounds associated with this target face.
     * It is mapped to the "targetFace" field in the TrainingRoundEntity class.
     * @OneToMany is a JPA annotation that indicates that this is the one side of a one-to-many relationship.
     * The mappedBy attribute indicates the field in the TrainingRoundEntity class that owns the relationship.
     * @JsonIgnore is a Jackson annotation that indicates that this field should not be included when the object is serialized to JSON.
     */
    @OneToMany(mappedBy = "targetFace")
    @JsonIgnore
    private List<TrainingRoundEntity> trainingRounds;

    /**
     * Represents the name of the target face.
     */
    private String name;

    /**
     * Represents the diameter of the target face.
     */
    private int diameter;

    /**
     * Represents the type of the target face.
     */
    private String type;

    /**
     * Represents the list of distances for each ring in the target face.
     * @ElementCollection is a JPA annotation that indicates that this is a collection of basic type or embeddable class values.
     */
    @ElementCollection
    private List<Integer> ringsDistance;
}