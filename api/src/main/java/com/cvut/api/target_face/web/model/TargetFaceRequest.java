package com.cvut.api.target_face.web.model;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

import java.util.List;

/**
 * Represents a request to create or update a TargetFaceEntity.
 * This is a record class, which is a special kind of class in Java that is used to model immutable data.
 * Each field in the record class has an associated accessor method, but no mutator method.
 *
 * @NotEmpty is a Jakarta Validation annotation that indicates that the annotated element must not be null or empty.
 * @Min is a Jakarta Validation annotation that indicates that the annotated element must be a number whose value must be higher or equal to the specified minimum.
 * @NotNull is a Jakarta Validation annotation that indicates that the annotated element must not be null.
 */
public record TargetFaceRequest(
        /**
         * Represents the name of the target face.
         * It must not be null or empty.
         */
        @NotEmpty
        String name,

        /**
         * Represents the diameter of the target face.
         * It must be a positive integer.
         */
        @Min(1)
        int diameter,

        /**
         * Represents the type of the target face.
         * It must not be null or empty.
         */
        @NotEmpty
        String type,

        /**
         * Represents the list of distances for each ring in the target face.
         * It must not be null.
         */
        @NotNull
        List<Integer> ringsDistance) {
}