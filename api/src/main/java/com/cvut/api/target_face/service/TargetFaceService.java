package com.cvut.api.target_face.service;

import com.cvut.api.target_face.domain.model.TargetFaceEntity;
import com.cvut.api.target_face.domain.repository.TargetFaceRepository;
import com.cvut.api.target_face.web.model.TargetFaceRequest;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * TargetFaceService is a service class that provides operations related to TargetFaceEntity.
 * It uses TargetFaceRepository to perform database operations.
 *
 * @Service is a Spring annotation that marks this class as a service. This means that Spring manages this class and can inject it where needed.
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field that requires special handling. In this case, it generates a constructor that takes a TargetFaceRepository.
 */
@Service
@RequiredArgsConstructor
public class TargetFaceService {

    /**
     * The TargetFaceRepository that this service uses to perform database operations.
     */
    private final TargetFaceRepository targetFaceRepository;

    /**
     * Retrieves all TargetFaceEntities.
     *
     * @return a list of all TargetFaceEntities.
     */
    public List<TargetFaceEntity> getAll() {
        return targetFaceRepository.findAll();
    }

    /**
     * Retrieves a TargetFaceEntity by its id.
     *
     * @param id the id of the TargetFaceEntity to retrieve.
     * @return the TargetFaceEntity with the given id.
     * @throws EntityNotFoundException if no TargetFaceEntity with the given id could be found.
     */
    public TargetFaceEntity getById(UUID id) {
        Objects.requireNonNull(id, "Id must not be null");
        return targetFaceRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Target face not found"));
    }

    /**
     * Creates a new TargetFaceEntity.
     *
     * @param targetFaceRequest the request object that contains the details of the new TargetFaceEntity.
     * @return the created TargetFaceEntity.
     */
    public TargetFaceEntity create(TargetFaceRequest targetFaceRequest) {
        return targetFaceRepository.save(
                TargetFaceEntity.builder()
                        .name(targetFaceRequest.name())
                        .type(targetFaceRequest.type())
                        .diameter(targetFaceRequest.diameter())
                        .ringsDistance(targetFaceRequest.ringsDistance())
                        .build());
    }
}