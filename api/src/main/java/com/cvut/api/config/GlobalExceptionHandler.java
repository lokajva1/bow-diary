package com.cvut.api.config;

import com.cvut.api.common.exceptions.InvalidPasswordOrUserException;
import jakarta.persistence.EntityNotFoundException;
import org.postgresql.util.PSQLException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * GlobalExceptionHandler is a class that handles exceptions globally across the whole application.
 * It is annotated with @ControllerAdvice, which makes it a central point for exception handling across all @RequestMapping methods.
 *
 * Each method in this class is annotated with @ExceptionHandler and handles a specific type of exception.
 * The methods return a ResponseEntity containing a map of errors and an HTTP status code.
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * Handles IllegalArgumentException.
     *
     * @param ex the IllegalArgumentException.
     * @return a ResponseEntity containing the error message and the HTTP status code BAD_REQUEST.
     */
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Map<String, List<String>>> handleIllegalArgumentException(IllegalArgumentException ex) {
        return new ResponseEntity<>(getErrorsMap(List.of(ex.getMessage())), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles EntityNotFoundException.
     *
     * @param ex the EntityNotFoundException.
     * @return a ResponseEntity containing the error message and the HTTP status code BAD_REQUEST.
     */
    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Map<String, List<String>>> handleEntityNotFoundException(EntityNotFoundException ex) {
        return new ResponseEntity<>(getErrorsMap(List.of(ex.getMessage())), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles InvalidPasswordOrUserException.
     *
     * @param ex the InvalidPasswordOrUserException.
     * @return a ResponseEntity containing the error message and the HTTP status code UNAUTHORIZED.
     */
    @ExceptionHandler(InvalidPasswordOrUserException.class)
    public ResponseEntity<Map<String, List<String>>> handleUserNotExistExceptionOrUsernamePasswordDoesNotMatch(InvalidPasswordOrUserException ex) {
        return new ResponseEntity<>(getErrorsMap(List.of(ex.getMessage())), HttpStatus.UNAUTHORIZED);
    }

    /**
     * Handles NullPointerException.
     *
     * @param ex the NullPointerException.
     * @return a ResponseEntity containing the error message and the HTTP status code BAD_REQUEST.
     */
    @ExceptionHandler(NullPointerException.class)
    public ResponseEntity<Map<String, List<String>>> handleNullPointerException(NullPointerException ex) {
        return new ResponseEntity<>(getErrorsMap(List.of(ex.getMessage())), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles PSQLException.
     *
     * @param ex the PSQLException.
     * @return a ResponseEntity containing the error message and the HTTP status code BAD_REQUEST.
     */
    @ExceptionHandler(PSQLException.class)
    public ResponseEntity<Map<String, List<String>>> handlePSQLException(PSQLException ex) {
        return new ResponseEntity<>(getErrorsMap(List.of(ex.getMessage())), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles MethodArgumentNotValidException.
     * This exception is thrown when validation on an argument annotated with @Valid fails.
     *
     * @param ex the MethodArgumentNotValidException.
     * @return a ResponseEntity containing the error messages and the HTTP status code BAD_REQUEST.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<String, List<String>>> handleValidationExceptions(MethodArgumentNotValidException ex) {
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream().map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.toList());
        return new ResponseEntity<>(getErrorsMap(errors), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Returns a map containing the provided list of errors.
     *
     * @param errors the list of errors.
     * @return a map containing the errors.
     */
    private Map<String, List<String>> getErrorsMap(List<String> errors) {
        Map<String, List<String>> errorResponse = new HashMap<>();
        errorResponse.put("errors", errors);
        return errorResponse;
    }
}