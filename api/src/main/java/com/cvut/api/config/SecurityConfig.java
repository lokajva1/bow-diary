package com.cvut.api.config;

import com.cvut.api.common.security.filter.JwtAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * This is a configuration class for Spring Security.
 * It uses the @Configuration annotation to indicate that it's a configuration class,
 * and the @EnableWebSecurity annotation to enable Spring Security's web security support.
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    @Lazy
    private JwtAuthorizationFilter jwtAuthorizationFilter ;
    /**
     * This method configures the SecurityFilterChain, which is a chain of filters that Spring Security uses to apply security to requests.
     * It uses the @Bean annotation to indicate that a bean should be created from the method.
     *
     * @param http an HttpSecurity instance, which is used to configure security at the web request level.
     * @return a SecurityFilterChain instance built from the HttpSecurity instance.
     * @throws Exception if an error occurs when configuring the SecurityFilterChain.
     */
    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(requests -> requests
                        .requestMatchers(HttpMethod.GET, "/actuator/health", "/actuator/info").permitAll()
                        .requestMatchers(HttpMethod.POST, "/api/user/registration", "/api/user/login").permitAll()
                        .anyRequest()
                        .authenticated())
                .csrf(AbstractHttpConfigurer::disable)
                .sessionManagement(sess -> sess.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .addFilterBefore(jwtAuthorizationFilter, UsernamePasswordAuthenticationFilter.class)
                .httpBasic(Customizer.withDefaults());

        return http.build();
    }

    /**
     * This method creates an instance of BCryptPasswordEncoder, which is a PasswordEncoder implementation that uses the BCrypt strong hashing function.
     * It uses the @Bean annotation to indicate that a bean should be created from the method.
     *
     * @return an instance of BCryptPasswordEncoder.
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
