package com.cvut.api.common.security.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import javax.crypto.spec.SecretKeySpec;

import java.security.Key;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;

/**
 * JwtService is a service class that provides methods for creating, validating, and extracting information from JWT tokens.
 *
 * @Service is a Spring annotation that indicates that the decorated class is a service.
 * A service is a class in the business layer that encapsulates business logic, controlling transactions and coordinating responses in the implementation of its operations.
 */
@Service
public class JwtService {

    /**
     * The expiration time of the JWT token in milliseconds.
     */
    private final int EXPIRATION_TIME = 1000 * 60 * 60;

    /**
     * The key of the authorities claim in the JWT token.
     */
    private final String AUTHORITIES = "authorities";

    /**
     * The secret key used to sign the JWT token.
     * Replace with your actual secret key.
     */
    private final String SECRET_KEY = "mySecretKey";

    /**
     * Returns the signing key derived from the secret key.
     *
     * @return the signing key.
     */
    private Key getSigningKey() {
        byte[] keyBytes = Base64.getDecoder().decode(SECRET_KEY);
        return new SecretKeySpec(keyBytes, SignatureAlgorithm.HS256.getJcaName());
    }

    /**
     * Creates a JWT token for the provided UserDetails.
     *
     * @param userDetails the UserDetails.
     * @return the JWT token.
     */
    public String createToken(UserDetails userDetails) {
        String username = userDetails.getUsername();
        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
        return Jwts.builder()
                .setSubject(username)
                .claim(AUTHORITIES, authorities)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS256, getSigningKey())
                .compact();
    }

    /**
     * Checks if the provided JWT token has expired.
     *
     * @param token the JWT token.
     * @return true if the token has expired, false otherwise.
     */
    public Boolean hasTokenExpired(String token) {
        return Jwts.parser()
                .setSigningKey(getSigningKey())
                .parseClaimsJws(token)
                .getBody()
                .getExpiration()
                .before(new Date());
    }

    /**
     * Validates the provided JWT token against the provided UserDetails.
     * The token is valid if the username in the token matches the username in the UserDetails and the token has not expired.
     *
     * @param token the JWT token.
     * @param userDetails the UserDetails.
     * @return true if the token is valid, false otherwise.
     */
    public Boolean validateToken(String token, UserDetails userDetails) {
        String username = extractUsername(token);
        return (userDetails.getUsername().equals(username) && !hasTokenExpired(token));
    }

    /**
     * Extracts the username from the provided JWT token.
     *
     * @param token the JWT token.
     * @return the username.
     */
    public String extractUsername(String token) {
        return Jwts.parser()
                .setSigningKey(getSigningKey())
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }
}