package com.cvut.api.common.security.filter;

import com.cvut.api.common.security.service.JwtService;
import com.cvut.api.user_account.service.UserAccountService;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

/**
 * JwtAuthorizationFilter is a custom filter that extends OncePerRequestFilter.
 * It is used to authorize requests based on JWT tokens.
 *
 * @Component is a Spring annotation that indicates that the decorated class is a component.
 * A component is a class that Spring manages as a bean in the application context.
 *
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with required fields.
 * Required fields are final fields and fields with constraints such as @NonNull.
 */
@Component
@RequiredArgsConstructor
public class JwtAuthorizationFilter extends OncePerRequestFilter {

    /**
     * The prefix of the token in the Authorization header.
     */
    private static final String HEADER_TOKEN_PREFIX = "Bearer ";

    /**
     * The key of the Authorization header.
     */
    private static final String HEADER_AUTHORIZATION_KEY = "Authorization";

    /**
     * UserAccountService is used to load user details.
     */
    private final UserAccountService userService;

    /**
     * JwtService is used to extract the username from the token and validate the token.
     */
    private final JwtService jwtService;

    /**
     * Filters the request based on the JWT token in the Authorization header.
     * If the token is valid, the authentication is set in the SecurityContext.
     *
     * @param request the HttpServletRequest.
     * @param response the HttpServletResponse.
     * @param filterChain the FilterChain.
     * @throws ServletException if the request could not be handled.
     * @throws IOException if an input or output error is detected when the servlet handles the request.
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = request.getHeader(HEADER_AUTHORIZATION_KEY);
        if (authorizationHeader != null && authorizationHeader.startsWith(HEADER_TOKEN_PREFIX)) {
            String token = authorizationHeader.replace(HEADER_TOKEN_PREFIX, "");
            String username = jwtService.extractUsername(token);

            if (username != null) {
                UserDetails userDetails = userService.loadUserByUsername(username);
                if (jwtService.validateToken(token, userDetails)) {
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        }
        filterChain.doFilter(request, response);
    }
}