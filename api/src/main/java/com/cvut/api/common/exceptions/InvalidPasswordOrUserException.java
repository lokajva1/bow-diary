package com.cvut.api.common.exceptions;

/**
 * InvalidPasswordOrUserException is a custom exception class that extends Throwable.
 * It is thrown when a user tries to authenticate with an invalid username or password.
 *
 * The class has a single constructor that accepts a string message. This message is passed to the superclass constructor.
 */
public class InvalidPasswordOrUserException extends Throwable {
    /**
     * Constructs a new InvalidPasswordOrUserException with the specified detail message.
     *
     * @param s the detail message. The detail message is saved for later retrieval by the Throwable.getMessage() method.
     */
    public InvalidPasswordOrUserException(String s) {
        super(s);
    }
}
