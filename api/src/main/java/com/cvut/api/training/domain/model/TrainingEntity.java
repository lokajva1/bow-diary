package com.cvut.api.training.domain.model;

import com.cvut.api.bow.domain.model.BowEntity;
import com.cvut.api.common.jpa.AbstractEntity;
import com.cvut.api.training_round.domain.model.TrainingRoundEntity;
import com.cvut.api.user_account.domain.model.UserAccountEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.List;

/**
 * Represents a training session in the system.
 * This is an entity class in the JPA sense, and is mapped to the "training" table in the database.
 *
 * @Getter is a Lombok annotation that generates getter methods for all fields.
 * @Setter is a Lombok annotation that generates setter methods for all fields.
 * @Entity is a JPA annotation that marks this class as an entity. This means that it is a JPA managed class and is mapped to a database table.
 * @NoArgsConstructor is a Lombok annotation that generates a no-argument constructor.
 * @AllArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field in the class.
 * @Builder is a Lombok annotation that generates a builder for this class.
 * @Table is a JPA annotation that specifies the name of the database table that this entity is mapped to.
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "training")
public class TrainingEntity extends AbstractEntity {

    /**
     * Represents the name of the training session.
     */
    private String name;

    /**
     * Represents the user associated with this training session.
     * It is mapped to the "user_id" field in the UserAccountEntity class.
     * @ManyToOne is a JPA annotation that indicates that this is the many side of a many-to-one relationship.
     * @JoinColumn indicates the column used for joining an entity association or element collection.
     * @JsonIgnore is a Jackson annotation that indicates that this field should not be included when the object is serialized to JSON.
     */
    @ManyToOne()
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private UserAccountEntity user;

    /**
     * Represents the list of training rounds associated with this training session.
     * It is mapped to the "training" field in the TrainingRoundEntity class.
     * @OneToMany is a JPA annotation that indicates that this is the one side of a one-to-many relationship.
     * The cascade attribute indicates the operations that must be cascaded to the target of the association.
     */
    @OneToMany(mappedBy = "training", cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<TrainingRoundEntity> rounds;

    /**
     * Represents the bow associated with this training session.
     * It is mapped to the "bow_id" field in the BowEntity class.
     * @ManyToOne is a JPA annotation that indicates that this is the many side of a many-to-one relationship.
     * @JoinColumn indicates the column used for joining an entity association or element collection.
     */
    @ManyToOne()
    @JoinColumn(name = "bow_id", nullable = false)
    private BowEntity bow;

    /**
     * Represents the date of the training session.
     */
    private Date date;
}