package com.cvut.api.training.service;

import com.cvut.api.bow.domain.model.BowEntity;
import com.cvut.api.bow.service.BowService;
import com.cvut.api.round_end.domain.model.RoundEndEntity;
import com.cvut.api.target_face.domain.model.TargetFaceEntity;
import com.cvut.api.target_face.domain.repository.TargetFaceRepository;
import com.cvut.api.training.domain.model.TrainingEntity;
import com.cvut.api.training.domain.repository.TrainingRepository;
import com.cvut.api.training.web.model.TrainingRequest;
import com.cvut.api.training_round.domain.model.TrainingRoundEntity;
import com.cvut.api.training_round.web.model.TrainingRoundRequest;
import com.cvut.api.user_account.domain.model.UserAccountEntity;
import com.cvut.api.user_account.service.UserAccountService;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.constraints.NotNull;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import java.util.UUID;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * TrainingService is a service class that provides business logic for operations related to TrainingEntity.
 * It uses UserAccountService, TrainingRepository, TargetFaceRepository, and BowService to perform these operations.
 *
 * @Service is a Spring annotation that marks this class as a service. This means that Spring manages this class and can inject it where needed.
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field that requires special handling.
 */
@Service
@RequiredArgsConstructor
public class TrainingService {

    /**
     * The UserAccountService that this service uses to perform operations related to UserAccountEntity.
     */
    private final UserAccountService userAccountService;

    /**
     * The TrainingRepository that this service uses to perform database operations on TrainingEntity.
     */
    private final TrainingRepository trainingRepository;

    /**
     * The TargetFaceRepository that this service uses to perform database operations on TargetFaceEntity.
     */
    private final TargetFaceRepository targetFaceRepository;

    /**
     * The BowService that this service uses to perform operations related to BowEntity.
     */
    private final BowService bowService;

    /**
     * Creates a new TrainingEntity.
     *
     * @param trainingRequest the request object that contains the details of the new TrainingEntity.
     * @return the created TrainingEntity.
     */
    public TrainingEntity createTraining(TrainingRequest trainingRequest) {
        UserAccountEntity userAccountEntity = userAccountService.getUser(UUID.fromString(trainingRequest.userId()));
        BowEntity bow = bowService.getBow(UUID.fromString(trainingRequest.bowId()));

        List<TrainingRoundEntity> rounds = trainingRequest.rounds()
                .stream()
                .map(trainingRoundRequest -> createTrainingRound(trainingRequest.rounds().indexOf(trainingRoundRequest) + 1, trainingRoundRequest))
                .toList();

        TrainingEntity training = TrainingEntity.builder()
                .rounds(rounds)
                .name(trainingRequest.name())
                .user(userAccountEntity)
                .bow(bow)
                .date(trainingRequest.date())
                .build();

        rounds.forEach(round -> round.setTraining(training));

        return trainingRepository.save(training);
    }

    /**
     * Retrieves a TrainingEntity by its id.
     *
     * @param id the id of the TrainingEntity to retrieve.
     * @return the TrainingEntity with the given id.
     * @throws EntityNotFoundException if a TrainingEntity with the given id is not found.
     */
    public TrainingEntity getTraining(UUID id) {
        Objects.requireNonNull(id, "Id must not be null");
        return trainingRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Training with id " + id + " not found"));
    }

    /**
     * Creates a new TrainingRoundEntity.
     *
     * @param roundNumber the round number of the new TrainingRoundEntity.
     * @param request the request object that contains the details of the new TrainingRoundEntity.
     * @return the created TrainingRoundEntity.
     */
    private TrainingRoundEntity createTrainingRound(int roundNumber, TrainingRoundRequest request) {
        TargetFaceEntity targetFace = targetFaceRepository.findById(UUID.fromString(request.targetFaceId()))
                .orElseThrow(() -> new EntityNotFoundException("Target face with id " + request.targetFaceId() + " not found"));

        List<RoundEndEntity> roundEnds = Stream.generate(() ->
                        RoundEndEntity.builder()
                                .arrowPositions(List.of())
                                .build())
                .limit(request.numberOfEnds())
                .toList();

        TrainingRoundEntity round = TrainingRoundEntity.builder()
                .roundNumber(roundNumber)
                .numberOfEnds(request.numberOfEnds())
                .arrowInEachEnd(request.arrowInEachEnd())
                .distance(request.distance())
                .note(request.note())
                .targetFace(targetFace)
                .roundEnds(roundEnds)
                .build();

        roundEnds.forEach(roundEnd -> roundEnd.setTrainingRound(round));

        return round;
    }

    /**
     * Deletes a TrainingEntity by its id.
     *
     * @param id the id of the TrainingEntity to delete.
     */
    public void deleteTraining(UUID id) {
        Objects.requireNonNull(id, "Id must not be null");
        TrainingEntity training = getTraining(id);
        trainingRepository.deleteById(training.getId());
    }
}