package com.cvut.api.training.web.model;

import com.cvut.api.training_round.web.model.TrainingRoundRequest;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Builder;

import java.util.Date;
import java.util.List;

/**
 * Represents a request to create or update a TrainingEntity.
 * This is a record class, which is a special kind of class in Java that is used to model immutable data.
 * Each field in the record class has an associated accessor method, but no mutator method.
 *
 * @Builder is a Lombok annotation that generates a builder for this class.
 *
 * @NotBlank is a Jakarta Validation annotation that indicates that the annotated element must not be null or empty.
 * @NotNull is a Jakarta Validation annotation that indicates that the annotated element must not be null.
 */
@Builder
public record TrainingRequest(
        /**
         * Represents the user id associated with the training.
         * It must not be null or empty.
         */
        @NotBlank
        String userId,

        /**
         * Represents the name of the training.
         * It must not be null or empty.
         */
        @NotBlank
        String name,

        /**
         * Represents the date of the training.
         * It must not be null.
         */
        @NotNull
        Date date,

        /**
         * Represents the bow id associated with the training.
         * It must not be null or empty.
         */
        @NotBlank
        String bowId,

        /**
         * Represents the list of training rounds associated with the training.
         * It must not be null.
         */
        @NotNull
        List<TrainingRoundRequest> rounds) {
}