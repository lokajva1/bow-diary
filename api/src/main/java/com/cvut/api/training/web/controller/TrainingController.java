package com.cvut.api.training.web.controller;

import com.cvut.api.training.domain.model.TrainingEntity;
import com.cvut.api.training.service.TrainingService;
import com.cvut.api.training.web.model.TrainingRequest;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID; 

/**
 * TrainingController is a REST controller that provides HTTP endpoints for operations related to TrainingEntity.
 * It uses TrainingService to perform the business logic.
 *
 * @RestController is a Spring annotation that marks this class as a REST controller. This means that Spring manages this class and can inject it where needed.
 * @RequestMapping is a Spring annotation that is used to map web requests onto specific handler classes and/or handler methods.
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field that requires special handling. In this case, it generates a constructor that takes a TrainingService.
 */
@RestController
@RequestMapping("/api/training")
@RequiredArgsConstructor
public class TrainingController {

    /**
     * The TrainingService that this controller uses to perform the business logic.
     */
    private final TrainingService trainingService;

    /**
     * Retrieves a TrainingEntity by its id.
     *
     * @param id the id of the TrainingEntity to retrieve.
     * @return the TrainingEntity with the given id.
     * @GetMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.GET).
     * @PathVariable is a Spring annotation which indicates that a method parameter should be bound to a URI template variable.
     */
    @GetMapping("/{id}")
    public ResponseEntity<TrainingEntity> getTraining(@PathVariable UUID id) {
        TrainingEntity trainingEntity = trainingService.getTraining(id);
        return ResponseEntity.ok(trainingEntity);
    }

    /**
     * Deletes a TrainingEntity by its id.
     *
     * @param id the id of the TrainingEntity to delete.
     * @return a ResponseEntity with no content.
     * @DeleteMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.DELETE).
     * @PathVariable is a Spring annotation which indicates that a method parameter should be bound to a URI template variable.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteTraining(@PathVariable UUID id) {
        trainingService.deleteTraining(id);
        return ResponseEntity.noContent().build();
    }

    /**
     * Creates a new TrainingEntity.
     *
     * @param trainingRequest the request object that contains the details of the new TrainingEntity.
     * @return the created TrainingEntity.
     * @PostMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.POST).
     * @Validated is a Spring annotation which indicates that a method parameter should be validated.
     * @RequestBody is a Spring annotation indicating a method parameter should be bound to the body of the web request.
     */
    @PostMapping
    public ResponseEntity<TrainingEntity> createTraining(@Validated @RequestBody TrainingRequest trainingRequest) {
        TrainingEntity trainingEntity = trainingService.createTraining(trainingRequest);
        return ResponseEntity.ok(trainingEntity);
    }
}