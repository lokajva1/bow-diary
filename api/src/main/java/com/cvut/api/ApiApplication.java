package com.cvut.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ApiApplication is the main entry point for the Spring Boot application.
 *
 * @SpringBootApplication is a convenience annotation that adds all of the following:
 * - @Configuration: Tags the class as a source of bean definitions for the application context.
 * - @EnableAutoConfiguration: Tells Spring Boot to start adding beans based on classpath settings, other beans, and various property settings.
 * - @ComponentScan: Tells Spring to look for other components, configurations, and services in the 'com.cvut.api' package, allowing it to find and register the controllers.
 */
@SpringBootApplication
public class ApiApplication {

    /**
     * The main method uses Spring Boot’s SpringApplication.run() method to launch an application.
     * This method returns an ApplicationContext where all the beans that were created either by your app or automatically added thanks to Spring Boot are.
     *
     * @param args array of strings used to accept command-line arguments.
     */
	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

}