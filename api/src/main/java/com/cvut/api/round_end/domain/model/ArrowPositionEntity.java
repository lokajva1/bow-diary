package com.cvut.api.round_end.domain.model;

import com.cvut.api.common.jpa.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.*;

/**
 * Represents the position of an arrow in a round end.
 * This is an entity class in the JPA sense, and is mapped to the "arrow_position" table in the database.
 *
 * @Getter is a Lombok annotation that generates getter methods for all fields.
 * @Setter is a Lombok annotation that generates setter methods for all fields.
 * @Entity is a JPA annotation that marks this class as an entity. This means that it is a JPA managed class and is mapped to a database table.
 * @NoArgsConstructor is a Lombok annotation that generates a no-argument constructor.
 * @AllArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field in the class.
 * @Builder is a Lombok annotation that generates a builder for this class.
 * @Table is a JPA annotation that specifies the name of the database table that this entity is mapped to.
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "arrow_position")
public class ArrowPositionEntity extends AbstractEntity {

    /**
     * Represents the round end that this arrow position is associated with.
     * It is mapped to the "round_end_id" column in the "arrow_position" table.
     * @ManyToOne is a JPA annotation that indicates that this is the many side of a many-to-one relationship.
     * @JoinColumn is a JPA annotation that indicates the column that is used for the foreign key.
     * @JsonIgnore is a Jackson annotation that indicates that this field should not be included when the object is serialized to JSON.
     */
    @ManyToOne
    @JoinColumn(name = "round_end_id", nullable = false)
    @JsonIgnore
    private RoundEndEntity roundEnd;

    /**
     * Represents the x-coordinate of the arrow position.
     */
    private int x;

    /**
     * Represents the y-coordinate of the arrow position.
     */
    private int y;

    /**
     * Represents the score label of the arrow position.
     */
    private String scoreLabel;
}