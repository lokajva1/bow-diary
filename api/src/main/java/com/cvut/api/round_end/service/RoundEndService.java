package com.cvut.api.round_end.service;

import com.cvut.api.round_end.domain.model.RoundEndEntity;
import com.cvut.api.round_end.domain.repository.RoundEndRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.UUID;

/**
 * RoundEndService is a service class that provides operations related to RoundEndEntity.
 * It uses RoundEndRepository to perform database operations.
 *
 * @Service is a Spring annotation that marks this class as a service. This means that Spring manages this class and can inject it where needed.
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field that requires special handling. In this case, it generates a constructor that takes a RoundEndRepository.
 */
@Service
@RequiredArgsConstructor
public class RoundEndService {

    /**
     * The RoundEndRepository that this service uses to perform database operations.
     */
    private final RoundEndRepository roundEndRepository;

    /**
     * Retrieves a RoundEndEntity by its id.
     *
     * @param id the id of the RoundEndEntity to retrieve.
     * @return the RoundEndEntity with the given id.
     * @throws EntityNotFoundException if no RoundEndEntity with the given id could be found.
     */
    public RoundEndEntity getRoundEnd(UUID id) {
        Objects.requireNonNull(id, "Id must not be null");
        return roundEndRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Round end not found"));
    }

    /**
     * Updates a RoundEndEntity with new arrow positions.
     *
     * @param id the id of the RoundEndEntity to update.
     * @param roundEndEntity the RoundEndEntity that contains the new arrow positions.
     * @return the updated RoundEndEntity.
     */
    public RoundEndEntity updateRoundEnd(UUID id, RoundEndEntity roundEndEntity) {
        Objects.requireNonNull(id, "Id must not be null");
        RoundEndEntity existingRoundEnd = getRoundEnd(id);
        // Clear the existing list of arrow positions
        existingRoundEnd.getArrowPositions().clear();
        // Save the round end to delete the arrow positions from the database
        roundEndRepository.save(existingRoundEnd);
        // Set the round end to the new arrow positions
        roundEndEntity.getArrowPositions().forEach(arrowPosition -> arrowPosition.setRoundEnd(existingRoundEnd));
        // Add the new arrow positions to the list
        existingRoundEnd.getArrowPositions().addAll(roundEndEntity.getArrowPositions());
        // Save the round end to save the new arrow positions to the database
        return roundEndRepository.save(existingRoundEnd);
    }
}