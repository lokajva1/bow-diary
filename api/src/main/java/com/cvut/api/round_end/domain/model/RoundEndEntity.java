package com.cvut.api.round_end.domain.model;

import com.cvut.api.common.jpa.AbstractEntity;
import com.cvut.api.training_round.domain.model.TrainingRoundEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * Represents a round end in a training round.
 * This is an entity class in the JPA sense, and is mapped to the "round_end" table in the database.
 *
 * @Getter is a Lombok annotation that generates getter methods for all fields.
 * @Setter is a Lombok annotation that generates setter methods for all fields.
 * @Entity is a JPA annotation that marks this class as an entity. This means that it is a JPA managed class and is mapped to a database table.
 * @NoArgsConstructor is a Lombok annotation that generates a no-argument constructor.
 * @AllArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field in the class.
 * @Builder is a Lombok annotation that generates a builder for this class.
 * @Table is a JPA annotation that specifies the name of the database table that this entity is mapped to.
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "round_end")
public class RoundEndEntity extends AbstractEntity {

    /**
     * Represents the training round that this round end is associated with.
     * It is mapped to the "training_round_id" column in the "round_end" table.
     * @ManyToOne is a JPA annotation that indicates that this is the many side of a many-to-one relationship.
     * @JoinColumn is a JPA annotation that indicates the column that is used for the foreign key.
     * @JsonIgnore is a Jackson annotation that indicates that this field should not be included when the object is serialized to JSON.
     */
    @ManyToOne
    @JoinColumn(name = "training_round_id", nullable = false)
    @JsonIgnore
    private TrainingRoundEntity trainingRound;

    /**
     * Represents the list of arrow positions associated with this round end.
     * It is mapped to the "roundEnd" field in the ArrowPositionEntity class.
     * @OneToMany is a JPA annotation that indicates that this is the one side of a one-to-many relationship.
     * The mappedBy attribute indicates the field in the ArrowPositionEntity class that owns the relationship.
     * The cascade attribute indicates that all operations that are applied to the round end are also applied to the associated arrow positions.
     * The orphanRemoval attribute indicates that when an arrow position is removed from the list, it is also deleted from the database.
     */
    @OneToMany(mappedBy = "roundEnd", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ArrowPositionEntity> arrowPositions;
}