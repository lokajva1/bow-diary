package com.cvut.api.round_end.web.controller;

import com.cvut.api.round_end.domain.model.RoundEndEntity;
import com.cvut.api.round_end.service.RoundEndService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.UUID; 

/**
 * RoundEndController is a REST controller that provides HTTP endpoints for operations related to RoundEndEntity.
 * It uses RoundEndService to perform the business logic.
 *
 * @RestController is a Spring annotation that marks this class as a REST controller. This means that Spring manages this class and can inject it where needed.
 * @RequestMapping is a Spring annotation that is used to map web requests onto specific handler classes and/or handler methods.
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field that requires special handling. In this case, it generates a constructor that takes a RoundEndService.
 */
@RestController
@RequestMapping("/api/round-end")
@RequiredArgsConstructor
public class RoundEndController {

    /**
     * The RoundEndService that this controller uses to perform the business logic.
     */
    private final RoundEndService roundEndService;

    /**
     * Retrieves a RoundEndEntity by its id.
     *
     * @param id the id of the RoundEndEntity to retrieve.
     * @return a ResponseEntity that contains the RoundEndEntity with the given id.
     * @GetMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.GET).
     * @PathVariable is a Spring annotation which indicates that a method parameter should be bound to a URI template variable.
     */
    @GetMapping("/{id}")
    public ResponseEntity<RoundEndEntity> getRoundEnd(@PathVariable UUID id) {
        RoundEndEntity trainingEntity = roundEndService.getRoundEnd(id);
        return ResponseEntity.ok(trainingEntity);
    }

    /**
     * Updates a RoundEndEntity with new arrow positions.
     *
     * @param id the id of the RoundEndEntity to update.
     * @param roundEndEntity the RoundEndEntity that contains the new arrow positions.
     * @return a ResponseEntity that contains the updated RoundEndEntity.
     * @PutMapping is a composed annotation that acts as a shortcut for @RequestMapping(method = RequestMethod.PUT).
     * @PathVariable is a Spring annotation which indicates that a method parameter should be bound to a URI template variable.
     * @Validated is a Spring annotation which indicates that a method parameter should be validated.
     * @RequestBody is a Spring annotation indicating a method parameter should be bound to the body of the web request.
     */
    @PutMapping("/{id}")
    public ResponseEntity<RoundEndEntity> updateRoundEnd(@PathVariable UUID id, @Validated @RequestBody RoundEndEntity roundEndEntity) {
        RoundEndEntity updatedRoundEnd = roundEndService.updateRoundEnd(id, roundEndEntity);
        return ResponseEntity.ok(updatedRoundEnd);
    }
}