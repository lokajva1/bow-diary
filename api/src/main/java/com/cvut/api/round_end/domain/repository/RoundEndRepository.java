package com.cvut.api.round_end.domain.repository;

import com.cvut.api.round_end.domain.model.RoundEndEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID; 

/**
 * RoundEndRepository is an interface that extends JpaRepository.
 * It is used to perform database operations on the RoundEndEntity.
 *
 * @Repository is a Spring annotation that indicates that the decorated class is a repository.
 * A repository is a mechanism for encapsulating storage, retrieval, and search behavior which emulates a collection of objects.
 *
 * JpaRepository is a JPA specific extension of Repository. It contains the full API of CrudRepository and PagingAndSortingRepository.
 * So it contains API for basic CRUD operations and also API for pagination and sorting.
 *
 * RoundEndEntity is the entity that this repository works with.
 * UUID is the id type of the entity this repository works with.
 */
@Repository
public interface RoundEndRepository extends JpaRepository<RoundEndEntity, UUID> {
}