package com.cvut.api.bow.domain.model;

import com.cvut.api.common.jpa.AbstractEntity;
import com.cvut.api.user_account.domain.model.UserAccountEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

/**
 * Represents the Bow entity in the application.
 * This class extends the AbstractEntity class.
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "bow")
public class BowEntity extends AbstractEntity {

    /**
     * Represents the name of the bow.
     */
    private String name;

    /**
     * Represents the user who owns the bow.
     * It is a many-to-one relationship with the UserAccountEntity.
     * The 'user_id' column in the 'bow' table is mapped to the 'id' column in the 'user_account' table.
     */
    @ManyToOne()
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private UserAccountEntity user;

    /**
     * Represents the type of the bow.
     * It is a many-to-one relationship with the BowTypeEntity.
     * The 'bow_type_id' column in the 'bow' table is mapped to the 'id' column in the 'bow_type' table.
     */
    @ManyToOne
    @JoinColumn(name = "bow_type_id", nullable = false)
    private BowTypeEntity bowType;

    /**
     * Represents the strength of the bow.
     */
    private int strength;

    /**
     * Represents any additional notes about the bow.
     */
    private String note;
}