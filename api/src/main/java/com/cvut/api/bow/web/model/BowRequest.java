package com.cvut.api.bow.web.model;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;

/**
 * Represents a request for creating or updating a BowEntity.
 * This is a record class in Java, which is a final, immutable class.
 * Each field in the record class corresponds to a component of the record.
 * The record class automatically provides several methods including equals(), hashCode(), toString(), and accessor methods for the components.
 *
 * @NotBlank is a Jakarta validation annotation that checks that the annotated string is not null and the trimmed length is greater than zero.
 * @Positive is a Jakarta validation annotation that checks that the annotated number is positive.
 */
public record BowRequest(
        /**
         * Represents the name of the bow.
         * It must not be blank.
         */
        @NotBlank String name,

        /**
         * Represents the id of the BowTypeEntity associated with the bow.
         * It must not be blank.
         */
        @NotBlank String bowTypeId,

        /**
         * Represents the strength of the bow.
         * It must be positive.
         */
        @Positive Integer strength,

        /**
         * Represents the note for the bow.
         * It can be null or blank.
         */
        String note
) {
}