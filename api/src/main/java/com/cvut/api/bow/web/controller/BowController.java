package com.cvut.api.bow.web.controller;

import com.cvut.api.bow.domain.model.BowEntity;
import com.cvut.api.bow.domain.model.BowTypeEntity;
import com.cvut.api.bow.service.BowService;
import com.cvut.api.bow.web.model.BowRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID; 

/**
 * BowController is a REST controller that provides endpoints for managing BowEntity and BowTypeEntity.
 * It uses BowService to perform the business logic.
 *
 * @RestController is a Spring annotation that indicates that the decorated class is a REST controller.
 * @RequestMapping("/api/bow") maps the URL of the endpoints to /api/bow.
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with required fields.
 * Required fields are final fields and fields with constraints such as @NonNull.
 */
@RestController
@RequestMapping("/api/bow")
@RequiredArgsConstructor
public class BowController {

    /**
     * BowService is used to perform the business logic.
     */
    private final BowService bowService;

    /**
     * Creates a new BowEntity and returns it.
     * The endpoint is mapped to POST /api/bow.
     * The BowRequest is validated before processing.
     *
     * @param bowEntity the BowRequest containing the details of the bow to be created.
     * @return the created BowEntity.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BowEntity createBow(@Validated @RequestBody BowRequest bowEntity) {
        return bowService.createBow(bowEntity);
    }

    /**
     * Retrieves a BowEntity using the provided id.
     * The endpoint is mapped to GET /api/bow/{id}.
     *
     * @param id the UUID of the BowEntity to be retrieved.
     * @return the retrieved BowEntity.
     */
    @GetMapping("/{id}")
    public BowEntity getBow(@PathVariable UUID id) {
        return bowService.getBow(id);
    }

    /**
     * Updates a BowEntity using the provided id and BowRequest and returns it.
     * The endpoint is mapped to PUT /api/bow/{id}.
     * The BowRequest is validated before processing.
     *
     * @param id the UUID of the BowEntity to be updated.
     * @param bowEntity the BowRequest containing the details of the bow to be updated.
     * @return the updated BowEntity.
     */
    @PutMapping("/{id}")
    public BowEntity updateBow(@PathVariable UUID id, @Validated @RequestBody BowRequest bowEntity) {
        return bowService.updateBow(id, bowEntity);
    }

    /**
     * Deletes a BowEntity using the provided id.
     * The endpoint is mapped to DELETE /api/bow/{id}.
     *
     * @param id the UUID of the BowEntity to be deleted.
     */
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteBow(@PathVariable UUID id) {
        bowService.deleteBow(id);
    }

    /**
     * Retrieves all BowEntity.
     * The endpoint is mapped to GET /api/bow.
     *
     * @return a list of all BowEntity.
     */
    @GetMapping
    public List<BowEntity> getAllBows() {
        return bowService.getAllBows();
    }

    /**
     * Retrieves all BowTypeEntity.
     * The endpoint is mapped to GET /api/bow/types.
     *
     * @return a list of all BowTypeEntity.
     */
    @GetMapping("/types")
    public List<BowTypeEntity> getAllBowTypes() {
        return bowService.getAllBowTypes();
    }
}