package com.cvut.api.bow.domain.repository;

import com.cvut.api.bow.domain.model.BowEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID; 

/**
 * BowRepository is an interface that extends JpaRepository.
 * It provides methods to perform CRUD operations on BowEntity.
 *
 * @Repository is a Spring annotation that indicates that the decorated class is a repository.
 * A repository is a mechanism for encapsulating storage, retrieval, and search behavior which emulates a collection of objects.
 *
 * JpaRepository is a JPA specific extension of Repository. It contains the full API of CrudRepository and PagingAndSortingRepository.
 * So it contains API for basic CRUD operations and also API for pagination and sorting.
 */
@Repository
public interface BowRepository extends JpaRepository<BowEntity, UUID> {
}