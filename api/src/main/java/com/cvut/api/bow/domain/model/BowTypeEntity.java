package com.cvut.api.bow.domain.model;

import com.cvut.api.common.jpa.AbstractEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

/**
 * Represents the BowType entity in the application.
 * This class extends the AbstractEntity class.
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "bow_type")
public class BowTypeEntity extends AbstractEntity {

    /**
     * Represents the name of the bow type.
     */
    private String name;

    /**
     * Represents the list of bows of this type.
     * It is a one-to-many relationship with the BowEntity.
     * The 'bowType' field in the 'BowEntity' is mapped to this entity.
     */
    @JsonIgnore
    @OneToMany(mappedBy = "bowType")
    private List<BowEntity> bows;
}