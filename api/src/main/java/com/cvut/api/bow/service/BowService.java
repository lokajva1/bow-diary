package com.cvut.api.bow.service;

import com.cvut.api.bow.domain.model.BowEntity;
import com.cvut.api.bow.domain.model.BowTypeEntity;
import com.cvut.api.bow.domain.repository.BowRepository;
import com.cvut.api.bow.domain.repository.BowTypeRepository;
import com.cvut.api.bow.web.model.BowRequest;
import jakarta.persistence.EntityNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID; 
/**
 * BowService is a service class that provides methods for managing BowEntity and BowTypeEntity.
 * It uses BowRepository and BowTypeRepository to perform CRUD operations.
 *
 * @Service is a Spring annotation that indicates that the decorated class is a service.
 * A service is a class in the business layer that encapsulates business logic, controlling transactions and coordinating responses in the implementation of its operations.
 *
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with required fields.
 * Required fields are final fields and fields with constraints such as @NonNull.
 */
@Service
@RequiredArgsConstructor
public class BowService {

    private final BowRepository bowRepository;
    private final BowTypeRepository bowTypeRepository;

    /**
     * Creates a new BowEntity and saves it in the database.
     * The BowTypeEntity is retrieved from the database using the provided bowTypeId.
     * If the BowTypeEntity is not found, an EntityNotFoundException is thrown.
     *
     * @param bowEntity the BowRequest containing the details of the bow to be created.
     * @return the created BowEntity.
     * @throws EntityNotFoundException if the BowTypeEntity is not found.
     */
    public BowEntity createBow(BowRequest bowEntity) {
        Objects.requireNonNull(bowEntity.bowTypeId(), "BowType id is required");

        BowTypeEntity bowTypeEntity = bowTypeRepository.findById(UUID.fromString(bowEntity.bowTypeId()))
                .orElseThrow(() -> new EntityNotFoundException("BowType with id " + bowEntity.bowTypeId() + " not found"));

        BowEntity bow = BowEntity.builder()
                .name(bowEntity.name())
                .bowType(bowTypeEntity)
                .strength(bowEntity.strength())
                .note(bowEntity.note())
                .build();

        return bowRepository.save(bow);
    }

    /**
     * Retrieves a BowEntity from the database using the provided id.
     * If the BowEntity is not found, an EntityNotFoundException is thrown.
     *
     * @param id the UUID of the BowEntity to be retrieved.
     * @return the retrieved BowEntity.
     * @throws EntityNotFoundException if the BowEntity is not found.
     */
    public BowEntity getBow(@NonNull UUID id) {
        Objects.requireNonNull(id, "Id must not be null");
        return bowRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Bow with id " + id + " not found"));
    }

    /**
     * Updates a BowEntity in the database using the provided id and BowRequest.
     * The BowTypeEntity is retrieved from the database using the provided bowTypeId in the BowRequest.
     * If the BowTypeEntity is not found, an EntityNotFoundException is thrown.
     *
     * @param id the UUID of the BowEntity to be updated.
     * @param bowEntity the BowRequest containing the details of the bow to be updated.
     * @return the updated BowEntity.
     * @throws EntityNotFoundException if the BowTypeEntity or BowEntity is not found.
     */
    public BowEntity updateBow(UUID id, BowRequest bowEntity) {
        Objects.requireNonNull(id, "Id must not be null");
        Objects.requireNonNull(bowEntity.bowTypeId(), "BowType id is required");

        BowTypeEntity bowTypeEntity = bowTypeRepository.findById(UUID.fromString(bowEntity.bowTypeId()))
                .orElseThrow(() -> new EntityNotFoundException("BowType with id " + bowEntity.bowTypeId() + " not found"));

        BowEntity bow = getBow(id);
        bow.setName(bowEntity.name());
        bow.setStrength(bowEntity.strength());
        bow.setNote(bowEntity.note());
        bow.setBowType(bowTypeEntity);

        return bowRepository.save(bow);
    }

    /**
     * Deletes a BowEntity from the database using the provided id.
     * If the BowEntity is not found, an EntityNotFoundException is thrown.
     *
     * @param id the UUID of the BowEntity to be deleted.
     * @throws EntityNotFoundException if the BowEntity is not found.
     */
    public void deleteBow( UUID id) {
        Objects.requireNonNull(id, "Id must not be null");
        BowEntity bow = getBow(id);
        bowRepository.deleteById(bow.getId());
    }

    /**
     * Retrieves all BowEntity from the database.
     *
     * @return a list of all BowEntity.
     */
    public List<BowEntity> getAllBows() {
        return bowRepository.findAll();
    }

    /**
     * Retrieves all BowTypeEntity from the database.
     *
     * @return a list of all BowTypeEntity.
     */
    public List<BowTypeEntity> getAllBowTypes() {
        return bowTypeRepository.findAll();
    }
}