package com.cvut.api.user_account.web.model;

import jakarta.validation.constraints.Size;

/**
 * Represents a request containing a user's username and password.
 * This is a record class, which is a special kind of class in Java that is used to model immutable data.
 * Each field in the record class has an associated accessor method, but no mutator method.
 *
 * @Size is a Jakarta Validation annotation that validates that the annotated string has a size between the specified boundaries.
 */
public record UserAccountRequest(
        /**
         * Represents the username of the user.
         * It must be between 3 and 15 characters long.
         */
        @Size(min = 3, max = 15) String username,

        /**
         * Represents the password of the user.
         * It must be between 3 and 15 characters long.
         */
        @Size(min = 3, max = 15) String password
) {
}