package com.cvut.api.user_account.service;

import com.cvut.api.bow.domain.model.BowEntity;
import com.cvut.api.common.exceptions.InvalidPasswordOrUserException;
import com.cvut.api.common.security.service.JwtService;
import com.cvut.api.training.domain.model.TrainingEntity;
import com.cvut.api.user_account.domain.model.UserAccountEntity;
import com.cvut.api.user_account.domain.repository.UserAccountRepository;
import com.cvut.api.user_account.web.model.TokenResponse;
import com.cvut.api.user_account.web.model.UserAccountRequest;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID; 

/**
 * UserAccountService is a service class that provides business logic for operations related to UserAccountEntity.
 * It uses UserAccountRepository, PasswordEncoder, and JwtService to perform these operations.
 *
 * @Service is a Spring annotation that marks this class as a service. This means that Spring manages this class and can inject it where needed.
 * @AllArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field that requires special handling.
 */
@Service
@AllArgsConstructor
public class UserAccountService implements UserDetailsService {

    /**
     * The UserAccountRepository that this service uses to perform database operations on UserAccountEntity.
     */
    private final UserAccountRepository userAccountRepository;

    /**
     * The PasswordEncoder that this service uses to encode passwords.
     */
    private final PasswordEncoder passwordEncoder;

    /**
     * The JwtService that this service uses to create JWT tokens.
     */
    private final JwtService jwtService;

    /**
     * Loads a user by its username.
     *
     * @param username the username of the user to load.
     * @return the UserDetails of the user with the given username.
     * @throws UsernameNotFoundException if a user with the given username is not found.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userAccountRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Username does not exist"));
    }

    /**
     * Retrieves a UserAccountEntity by its id.
     *
     * @param id the id of the UserAccountEntity to retrieve.
     * @return the UserAccountEntity with the given id.
     * @throws EntityNotFoundException if a UserAccountEntity with the given id is not found.
     */
    public UserAccountEntity getUser(UUID id) {
        Objects.requireNonNull(id, "Id must not be null");
        return userAccountRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("User with id " + id + " not found"));
    }

    /**
     * Logs in a user.
     *
     * @param userAccountRequest the request containing the username and password of the user to log in.
     * @return a TokenResponse containing the id and JWT token of the logged in user.
     * @throws InvalidPasswordOrUserException if the username does not exist or the password is incorrect.
     */
    public TokenResponse login(UserAccountRequest userAccountRequest) throws InvalidPasswordOrUserException {
        Objects.requireNonNull(userAccountRequest.username(), "Username cannot be null");
        Objects.requireNonNull(userAccountRequest.password(), "Password cannot be null");
        Optional<UserAccountEntity> userAccount = userAccountRepository.findByUsername(userAccountRequest.username());
        if (userAccount.isEmpty() || !passwordEncoder.matches(userAccountRequest.password(), userAccount.get().getPassword())) {
            throw new InvalidPasswordOrUserException("Username does not exist or password is incorrect");
        }
        UUID userId = userAccount.get().getId();
        String token = jwtService.createToken(userAccount.get());
        return new TokenResponse(userId, token);
    }

    /**
     * Registers a new user.
     *
     * @param userAccountRequest the request containing the username and password of the user to register.
     * @throws IllegalArgumentException if the username already exists.
     */
    public void register(UserAccountRequest userAccountRequest) {
        Objects.requireNonNull(userAccountRequest.username(), "Username cannot be null");
        Objects.requireNonNull(userAccountRequest.password(), "Password cannot be null");
        if (userAccountRepository.findByUsername(userAccountRequest.username()).isPresent()) {
            throw new IllegalArgumentException("Username already exists");
        }
        UserAccountEntity userAccount = new UserAccountEntity(userAccountRequest.username(), passwordEncoder.encode(userAccountRequest.password()));
        userAccountRepository.save(userAccount);
    }

    /**
     * Retrieves the trainings of a user.
     *
     * @param id the id of the user whose trainings to retrieve.
     * @return a list of TrainingEntity of the user with the given id.
     */
    public List<TrainingEntity> getUserTrainings(UUID  id) {
        Objects.requireNonNull(id, "Id must not be null");
        UserAccountEntity userAccount = getUser(id);
        return userAccount.getTrainings();
    }

    /**
     * Retrieves the bows of a user.
     *
     * @param id the id of the user whose bows to retrieve.
     * @return a list of BowEntity of the user with the given id.
     */
    public List<BowEntity> getUserBows(UUID id) {
        Objects.requireNonNull(id, "Id must not be null");
        UserAccountEntity userAccount = getUser(id);
        return userAccount.getBows();
    }
}