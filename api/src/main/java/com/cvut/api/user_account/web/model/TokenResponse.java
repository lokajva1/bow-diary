package com.cvut.api.user_account.web.model;

import lombok.NonNull;

import java.util.UUID; 

/**
 * Represents a response containing a user's id and a token.
 * This is a record class, which is a special kind of class in Java that is used to model immutable data.
 * Each field in the record class has an associated accessor method, but no mutator method.
 *
 * @NonNull is a Lombok annotation that indicates that the annotated element must not be null.
 */
public record TokenResponse(
        /**
         * Represents the user's id.
         * It must not be null.
         */
        @NonNull UUID userId,

        /**
         * Represents the token associated with the user.
         * It must not be null.
         */
        @NonNull String token) {
}