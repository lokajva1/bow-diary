package com.cvut.api.user_account.domain.repository;

import com.cvut.api.user_account.domain.model.UserAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID; 

/**
 * UserAccountRepository is an interface that extends JpaRepository.
 * It is used to perform database operations on the UserAccountEntity.
 *
 * JpaRepository is a JPA specific extension of Repository. It contains the full API of CrudRepository and PagingAndSortingRepository.
 * So it contains API for basic CRUD operations and also API for pagination and sorting.
 *
 * UserAccountEntity is the entity that this repository works with.
 * UUID is the id type of the entity this repository works with.
 *
 * @Repository is a Spring annotation that marks this class as a repository. This means that Spring manages this class and can inject it where needed.
 */
@Repository
public interface UserAccountRepository extends JpaRepository<UserAccountEntity, UUID> {

    /**
     * Custom query to find a UserAccountEntity by its username.
     *
     * @param username the username of the UserAccountEntity to retrieve.
     * @return an Optional that contains the UserAccountEntity if it exists, or empty if it does not.
     */
    @Query("SELECT u FROM UserAccountEntity u WHERE u.username = ?1")
    Optional<UserAccountEntity> findByUsername(String username);
}