package com.cvut.api.user_account.web.controller;

import com.cvut.api.bow.domain.model.BowEntity;
import com.cvut.api.common.exceptions.InvalidPasswordOrUserException;
import com.cvut.api.training.domain.model.TrainingEntity;
import com.cvut.api.user_account.service.UserAccountService;
import com.cvut.api.user_account.web.model.UserAccountRequest;
import com.cvut.api.user_account.web.model.TokenResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID; 

/**
 * UserAccountController is a REST controller that handles HTTP requests related to UserAccountEntity.
 * It uses UserAccountService to perform these operations.
 *
 * @RestController is a Spring annotation that marks this class as a REST controller. This means that Spring manages this class and can inject it where needed.
 * @RequestMapping is a Spring annotation that maps HTTP requests to handler methods of MVC and REST controllers.
 * @RequiredArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field that requires special handling.
 */
@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserAccountController {

    /**
     * The UserAccountService that this controller uses to perform operations related to UserAccountEntity.
     */
    private final UserAccountService userAccountService;

    /**
     * Handles the POST /api/user/login endpoint.
     * Logs in a user and returns a token.
     *
     * @param userAccountRequest the request containing the username and password of the user to log in.
     * @return a ResponseEntity containing a TokenResponse with the id and JWT token of the logged in user.
     * @throws InvalidPasswordOrUserException if the username does not exist or the password is incorrect.
     */
    @PostMapping("/login")
    public ResponseEntity<TokenResponse> createToken(@Validated @RequestBody UserAccountRequest userAccountRequest) throws InvalidPasswordOrUserException {
        TokenResponse tokenResponse = userAccountService.login(userAccountRequest);
        return ResponseEntity.ok(tokenResponse);
    }

    /**
     * Handles the POST /api/user/registration endpoint.
     * Registers a new user.
     *
     * @param userAccountRequest the request containing the username and password of the user to register.
     * @return a ResponseEntity indicating that the user was registered successfully.
     */
    @PostMapping("/registration")
    // not yet used in frontend project
    public ResponseEntity<?> registerUser(@Validated @RequestBody UserAccountRequest userAccountRequest) {
        userAccountService.register(userAccountRequest);
        return ResponseEntity.ok().build();
    }

    /**
     * Handles the GET /api/user/{id}/trainings endpoint.
     * Retrieves the trainings of a user.
     *
     * @param id the id of the user whose trainings to retrieve.
     * @return a ResponseEntity containing a list of TrainingEntity of the user with the given id.
     */
    @GetMapping("/{id}/trainings")
    public ResponseEntity<List<TrainingEntity>> getUserTrainings(@PathVariable UUID id) {
        List<TrainingEntity> trainings = userAccountService.getUserTrainings(id);
        return ResponseEntity.ok(trainings);
    }

    /**
     * Handles the GET /api/user/{id}/bows endpoint.
     * Retrieves the bows of a user.
     *
     * @param id the id of the user whose bows to retrieve.
     * @return a ResponseEntity containing a list of BowEntity of the user with the given id.
     */
    @GetMapping("/{id}/bows")
    public ResponseEntity<List<BowEntity>> getUserBows(@PathVariable UUID id) {
        List<BowEntity> bows = userAccountService.getUserBows(id);
        return ResponseEntity.ok(bows);
    }
}