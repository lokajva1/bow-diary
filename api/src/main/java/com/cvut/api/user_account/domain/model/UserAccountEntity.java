package com.cvut.api.user_account.domain.model;

import com.cvut.api.bow.domain.model.BowEntity;
import com.cvut.api.common.jpa.AbstractEntity;
import com.cvut.api.training.domain.model.TrainingEntity;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
/**
 * Represents a user account in the system.
 * This is an entity class in the JPA sense, and is mapped to the "user_account" table in the database.
 *
 * @Getter is a Lombok annotation that generates getter methods for all fields.
 * @Setter is a Lombok annotation that generates setter methods for all fields.
 * @Entity is a JPA annotation that marks this class as an entity. This means that it is a JPA managed class and is mapped to a database table.
 * @NoArgsConstructor is a Lombok annotation that generates a no-argument constructor.
 * @AllArgsConstructor is a Lombok annotation that generates a constructor with one parameter for each field in the class.
 * @Builder is a Lombok annotation that generates a builder for this class.
 * @Table is a JPA annotation that specifies the name of the database table that this entity is mapped to.
 */
@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "user_account")
public class UserAccountEntity extends AbstractEntity implements UserDetails {

    /**
     * Represents the username of the user account.
     */
    private String username;

    /**
     * Represents the password of the user account.
     */
    private String password;

    /**
     * Represents the list of bows associated with this user account.
     * It is mapped to the "user" field in the BowEntity class.
     * @OneToMany is a JPA annotation that indicates that this is the one side of a one-to-many relationship.
     */
    @OneToMany(mappedBy = "user")
    private List<BowEntity> bows;

    /**
     * Represents the list of trainings associated with this user account.
     * It is mapped to the "user" field in the TrainingEntity class.
     * @OneToMany is a JPA annotation that indicates that this is the one side of a one-to-many relationship.
     */
    @OneToMany(mappedBy = "user")
    private List<TrainingEntity> trainings;

    /**
     * Returns the authorities granted to the user.
     * In this case, it returns an empty list as no specific authorities are granted.
     * @return a collection of granted authorities
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of();
    }

    /**
     * Constructs a new UserAccountEntity with the specified username and password.
     *
     * @param username the username of the user account
     * @param password the password of the user account
     */
    public UserAccountEntity(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Checks if the account is not expired.
     * @return true as the account is never considered expired
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Checks if the account is not locked.
     * @return true as the account is never considered locked
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Checks if the credentials are not expired.
     * @return true as the credentials are never considered expired
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Checks if the account is enabled.
     * @return true as the account is always considered enabled
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}